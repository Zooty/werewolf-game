using Configurations;
using Microsoft.EntityFrameworkCore.Design;

namespace DatabaseConnection {
    public class WerewolfDbContextFactory: IDesignTimeDbContextFactory<WerewolfDbContext> {
        public WerewolfDbContext CreateDbContext(string[] args) {
            string connectionString = PropertyContainer.GetInstance().GetProperty(PropertyContainer.PropertyKey.DbConnectionString);
            return new WerewolfDbContext(connectionString);
        }
        
        public WerewolfDbContext CreateDbContext() {
            return CreateDbContext(new string[0]);
        }
    }
}