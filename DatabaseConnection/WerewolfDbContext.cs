﻿using DatabaseConnection.Entities;
using Microsoft.EntityFrameworkCore;

namespace DatabaseConnection {
    public class WerewolfDbContext: DbContext {
        private readonly string connectionString;

        public DbSet<Player> Players { get; set; }
        public DbSet<GameSession> GameSessions { get; set; }
        public DbSet<PlayerGameSession> PlayerGameSessions { get; set; }

        public WerewolfDbContext(string connectionString) {
            this.connectionString = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseMySql(connectionString);
        }

        /*protected override void OnModelCreating(ModelBuilder modelBuilder) {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<PlayerGameSession>().HasKey(playerGameSession => new {playerGameSession.PlayerId, playerGameSession.GameSessionId});
            modelBuilder.Entity<PlayerGameSession>()
                .HasOne(playerGameSession => playerGameSession.Player)
                .WithMany(Player => Player.PlayerGameSessions)
                .HasForeignKey(playerGameSession => playerGameSession.PlayerId);
            modelBuilder.Entity<PlayerGameSession>()
                .HasOne(playerGameSession => playerGameSession.GameSession)
                .WithMany(gameSession => gameSession.PlayerGameSessions)
                .HasForeignKey(playerGameSession => playerGameSession.GameSessionId);
        }*/
    }
}