namespace DatabaseConnection.Entities {
	public class PlayerGameSession {
		public string Id { get; set; }
		public Player Player { get; set; }
		public GameSession GameSession { get; set; }
	}
}