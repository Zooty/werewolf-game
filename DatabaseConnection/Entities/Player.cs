namespace DatabaseConnection.Entities {
    public class Player {
        public ulong Id { get; set; }
        public int GameTimes { get; set; }
        public int WonTimes { get; set; }
        public int FirstBloodTimes { get; set; }
    }
}