using System;

namespace DatabaseConnection.Entities {
    public class GameSession {
        public string Id { get; set; }
        public DateTime Time { get; set; }
        public int Days { get; set; }
    }
}