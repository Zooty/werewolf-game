﻿using System;
using System.Threading.Tasks;
using Configurations;
using Discord;
using Discord.WebSocket;
using Werewolf_game.Persistency;
using static Configurations.PropertyContainer.PropertyKey;

[assembly: log4net.Config.XmlConfigurator(ConfigFile = "log4net.config")]

namespace Werewolf_game {
    internal static class Program {
        private static readonly log4net.ILog Logger =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static void Main(string[] args) => MainAsync(args).GetAwaiter().GetResult();

        private static async Task MainAsync(string[] args) {
            try {
                Logger.Info("TestApp started!");

                DatabaseWrapper.init();
                Logger.Debug("Database is initialized");
                
                /*WerewolfDbContext dbContext = new WerewolfDbContextFactory().CreateDbContext();
                dbContext.Database.EnsureCreated();

                List<Player> players = dbContext.Players.ToList();
                players.ForEach(player => dbContext.Players.Remove(player));
                dbContext.Players.Add(new Player{Id = 1});
                dbContext.SaveChanges();
                Player p = dbContext.Players.SingleOrDefault(player => player.Id == 1);
                dbContext.GameSessions.Add(new GameSession {Players = new List<Player> {p}, Time = DateTime.Now});
                dbContext.SaveChanges();*/

                var client = new DiscordSocketClient();

                client.Log += Log;

                string token = PropertyContainer.GetInstance().GetProperty(Token);
                await client.LoginAsync(TokenType.Bot, token);
                await client.StartAsync();

                var werewolfBot = new WerewolfBot.WerewolfBot(client);
                werewolfBot.Start();

                // Block this task until the program is closed.
                await Task.Delay(-1);
            }
            catch (Exception e) {
                Logger.Fatal($"Fatal error in main! Program will terminate! - {e.Message}", e);
                Environment.Exit(-1);
            }
        }

        private static Task Log(LogMessage msg) {
            switch (msg.Severity) {
                case LogSeverity.Critical:
                    Logger.Fatal(msg.Message);
                    break;
                case LogSeverity.Error:
                    Logger.Error(msg.Message);
                    break;
                case LogSeverity.Warning:
                    Logger.Warn(msg.Message);
                    break;
                case LogSeverity.Info:
                    Logger.Info(msg.Message);
                    break;
                case LogSeverity.Verbose:
                    Logger.Debug(msg.Message);
                    break;
                case LogSeverity.Debug:
                    Logger.Debug(msg.Message);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return Task.CompletedTask;
        }
    }
}