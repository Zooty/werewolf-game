using System;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using Werewolf_game.WerewolfBot;

namespace Werewolf_game.Extensions {
    public static class DiscordSocketClientExtension {
        /// <summary>
        /// Calls e whenever a (optional) userId clicks on a reaction on a specific messageId
        /// </summary>
        /// <param name="client">"this" makes it a extension method, therefore you can call it with DiscordClient.</param>
        /// <param name="e">The function which gets called when a (or the) user clicks on a reaction</param>
        /// <param name="emote">The specific reaction emote</param>
        /// <param name="eventWrapper">Using this wrapper to add the reaction event, instead of adding to the client directly.</param>
        /// <param name="messageId">the message id we want to listen for</param>
        /// <param name="userId">(optional) the user we want to listen for</param>
        /// <returns>The function id to unregister this event, whenever we are done with the user, call with DiscordClient.UnregisterOnSpecificReactionAdded()</returns>
        public static Func<Cacheable<IUserMessage, ulong>, ISocketMessageChannel, SocketReaction, Task>
            OnSpecificReactionAdded(this DiscordSocketClient client, Action<Cacheable<IUserMessage, ulong>, ISocketMessageChannel, SocketReaction> e, ulong messageId, IEmote emote, ulong userId = 0) {
            // https://discord.foxbot.me/docs/guides/concepts/events.html#cacheable
            var func = new Func<Cacheable<IUserMessage, ulong>, ISocketMessageChannel, SocketReaction, Task>(
                (messageCache, channel, reaction) => {
                    if (messageCache.Id != messageId)
                        return Task.CompletedTask;

                    if ((userId == 0 || reaction.UserId == userId) && (emote == null || Equals(reaction.Emote, emote)))
                        e(messageCache, channel, reaction);

                    return Task.CompletedTask;
                }
            );

            client.ReactionAdded += func;
            return func;
        }

        /// <summary>
        /// Calls e whenever a (optional) userId clicks on a reaction on a specific messageId
        /// </summary>
        /// <param name="client">"this" makes it a extension method, therefore you can call it with DiscordClient.</param>
        /// <param name="e">The function which gets called when a (or the) user clicks on a reaction</param>
        /// <param name="eventWrapper">Using this wrapper to add the reaction event, instead of adding to the client directly.</param>
        /// <param name="messageId">the message id we want to listen for</param>
        /// <param name="emote">The specific reaction emote</param>
        /// <param name="userId">(optional) the user we want to listen for</param>
        /// <returns>The function id to unregister this event, whenever we are done with the user, call with DiscordClient.UnregisterOnSpecificReactionAdded()</returns>
        public static Func<Cacheable<IUserMessage, ulong>, ISocketMessageChannel, SocketReaction, Task>
            OnSpecificReactionAdded(this DiscordSocketClient client, Action<Cacheable<IUserMessage, ulong>, ISocketMessageChannel, SocketReaction> e, SessionEventWrapper eventWrapper, ulong messageId, IEmote emote, ulong userId = 0) {
            // https://discord.foxbot.me/docs/guides/concepts/events.html#cacheable
            var func = new Func<Cacheable<IUserMessage, ulong>, ISocketMessageChannel, SocketReaction, Task>(
                (messageCache, channel, reaction) => {
                    if (messageCache.Id != messageId)
                        return Task.CompletedTask;

                    if ((userId == 0 || reaction.UserId == userId) && (emote == null || Equals(reaction.Emote, emote)))
                        e(messageCache, channel, reaction);

                    return Task.CompletedTask;
                }
            );

            eventWrapper.ReactionAdded += func;
            return func;
        }

        public static void UnregisterOnSpecificUserMessageReceived(this DiscordSocketClient client, Func<Cacheable<IUserMessage, ulong>, ISocketMessageChannel, SocketReaction, Task> func) {
            client.ReactionAdded -= func;
        }

        /// <summary>
        /// calls e whenever a specific user "userId" sends a message in (optional) "channelId"
        /// </summary>
        /// <param name="client">"this" makes it a extension method, therefore you can call it with DiscordClient.UnregisterOnSpecificReactionAdded()</param>
        /// <param name="e">The function which gets called when "userId" sends a message</param>
        /// <param name="userId">The user we want to get a message from</param>
        /// <param name="channelId">(optional) the channel we want that the user sends a message in</param>
        /// <returns>The function id to unregister this event, whenever we are done with the user, call with DiscordClient.UnregisterOnSpecificUserMessageReceived()</returns>
        public static Func<SocketMessage, Task> OnSpecificUserMessageReceived(this DiscordSocketClient client, Action<SocketMessage> e, ulong userId, ulong channelId = 0) {
            var ret = new Func<SocketMessage, Task>(message => {
                if (message.Author.Id != userId)
                    return Task.CompletedTask;

                if (channelId == 0 || message.Channel.Id == channelId)
                    e(message);

                return Task.CompletedTask;
            });

            client.MessageReceived += ret;

            return ret;
        }

        public static void UnregisterOnSpecificUserMessageReceived(this DiscordSocketClient client, Func<SocketMessage, Task> func) {
            client.MessageReceived -= func;
        }
    }
}