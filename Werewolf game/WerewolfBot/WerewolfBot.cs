using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Configurations;
using Discord;
using Discord.WebSocket;
using Werewolf_game.Persistency;
using Werewolf_game.Utils;

//For not awaited async stuff
#pragma warning disable 4014

namespace Werewolf_game.WerewolfBot {
    internal class WerewolfBot {
        private static readonly log4net.ILog Logger =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly DiscordSocketClient client;

        private readonly HashSet<GameSession> gameSessions = new HashSet<GameSession>();

        public WerewolfBot(DiscordSocketClient client) {
            this.client = client ?? throw new NoNullAllowedException();
        }

        /// <summary>
        /// Starts the bot.
        /// </summary>
        public void Start() {
            client.MessageReceived += MessageReceiveEvent;
            //TODO Enable when implemented
            //client.JoinedGuild += GuildJoinEvent;
            //CheckJoinedServers();
            Logger.Info("Werewolf bot has started!");
        }

        /// <summary>
        /// Terminates all running game sessions, and stops the bot.
        /// </summary>
        public void Stop() {
            foreach (var gameSession in gameSessions) {
                gameSession.StopGame();
            }

            client.MessageReceived -= MessageReceiveEvent;
            Logger.Info("Werewolf bot has stopped!");
        }

        private async Task MessageReceiveEvent(SocketMessage msg) {
            if (client.CurrentUser.Id != msg.Author.Id) {
                //If it starts with the preset command indicator, we handle it as a command. Otherwise, skip.

                var command = Command.ParseCommand(msg);
                if (command.IsCommand) {
                    Logger.Debug($"Command received! Content: {command.CommandString}, Sender: {msg.Author}, channel: {msg.Channel}.");
                    await HandleCommand(command);
                }
            }
        }

        private async Task GuildJoinEvent(SocketGuild joinedGuild) {
            if (GuildData.RegisterGuild(joinedGuild) == null) {
                Logger.Info($"Bot has rejoined to {joinedGuild}.");
            }
            else {
                Logger.Info($"Bot has joined to {joinedGuild} for the first time.");
                //joinedGuild.Owner.SendMessageAsync(MessageRepository.GuildJoinInfoMessage);
            }
        }

        private void CheckJoinedServers() {
            foreach (var clientGuild in client.Guilds) {
                if (GuildData.GetGuild(clientGuild) == null) {
                    GuildJoinEvent(clientGuild);
                }
            }
        }

        private void ClearSession(GameSession session) {
            gameSessions.Remove(session);
            Logger.Debug($"Game session was cleared in channel: {session.Channel}");
        }

        private async Task HandleCommand(Command command) {
            if (command.IsCommand) {
                switch (command.CommandString.ToLower()) {
                    case "owo":
                        await command.Channel.SendMessageAsync("What's this?");
                        break;
                    case "startgame":
                        if (CanStart(command.Channel, command.Author)) { //No session in this channel
                            if (!await DiscordUtils.IsDMOpen(command.Author, "You have started a game!")) {
                                Logger.Debug($"Can't send DM messages to {command.Author} therefore can't join the game in {command.Channel}");
                                command.Channel.SendMessageAsync($"{command.Author.Username} can't accept Direct messages. Please check your DM settings!");
                            }
                            else {
                                try {
                                    Logger.Debug($"Starting new game on {command.Channel}");
                                    var sessionParameters = new SessionInitParameters();
                                    sessionParameters.ParseSettings(command.Arguments);
                                    Logger.Debug($"Session parameters for the new session: {sessionParameters}");
                                    
                                    var gameSession = new GameSession(client, command.Message, sessionParameters, ClearSession);
                                    gameSessions.Add(gameSession);
                                    gameSession.StartGame();
                                }
                                catch (Exception e) {
                                    Logger.Error($"Can't start game session for command: {command}", e);
                                }
                            }
                        }
                        else {
                            Logger.Debug($"Can't start game on {command.Channel}. Required conditions doesn't apply.");
                        }

                        break;
                    case "mystats":
                        Logger.Debug($"Getting user data for {command.Author}");
                        var userData = new UserData(command.Author);
                        Logger.Debug($"Fetched user data: {userData}");
                        command.Channel.SendMessageAsync(userData.StatusReport());
                        break;
                    case "resetproperties": //Technical command to reset property container
                        if (command.Author.Id == 69101994505142272) { //Me
                            PropertyContainer.Reset();
                            Logger.Info("Properties have been reset!");
                        }
                        break;
                }
            }
        }

        private bool CanStart(ISocketMessageChannel channel, SocketUser author) {
            if (gameSessions.Count(session => session.Channel == channel) > 0) {
                Logger.Debug($"{channel} already has a game session running.");
                channel.SendMessageAsync("There is already a game going on in this channel!");
                return false;
            }

            if (gameSessions.Any(session => session.IsPlaying(author))) {
                Logger.Debug($"{author} is already playing a game.");
                channel.SendMessageAsync("You are already playing a game!");
                return false;
            }

            if (!(channel is SocketTextChannel)) {
                Logger.Debug($"Channel is {channel.GetType()}. Game requires {typeof(SocketTextChannel)}");
                channel.SendMessageAsync("Game can only be started in a guild channel!");
                return false;
            }

            //TODO?: author has privilege..
            return true;
        }
    }
}