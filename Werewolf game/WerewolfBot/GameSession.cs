using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Configurations;
using Discord;
using Discord.Rest;
using Discord.WebSocket;
using log4net;
using Werewolf_game.Extensions;
using Werewolf_game.Persistency;
using Werewolf_game.Utils;
using Werewolf_game.WerewolfBot.Characters;
using Werewolf_game.WerewolfBot.Results;
using static Configurations.PropertyContainer.PropertyKey;
using static Werewolf_game.WerewolfBot.Characters.Character.Side;
using static Werewolf_game.WerewolfBot.Characters.Character.Type;

//for async not awaited
#pragma warning disable 4014

namespace Werewolf_game.WerewolfBot {
    public class GameSession {
        private static readonly ILog Logger =
            LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly HashSet<SocketUser> players = new HashSet<SocketUser>();
        private readonly DiscordSocketClient client;
        private readonly Action<GameSession> stopCallBack;
        private readonly SocketUser owner;
        private readonly SessionInitParameters initParameters;
        private readonly HashSet<Character> characters = new HashSet<Character>();
        private readonly GunnerResult gunnerShots = new GunnerResult();
        private readonly SessionEventWrapper sessionEvents;
        private readonly GameSessionData gameSessionData;

        private int day = 1;
        private Character masochistLynched;

        public ISocketMessageChannel Channel { get; }
        public bool IsRunning { get; private set; }

        public bool IsPlaying(SocketUser user) {
            return players.Contains(user);
        }

        /// <summary>
        /// Creates a new game session
        /// </summary>
        /// <param name="client">The Discord bot.</param>
        /// <param name="initialMessage">The Discord message that initiated the session.</param>
        /// <param name="initParameters">Init parameters for the session. Contains character quantity information and wait times.</param>
        /// <param name="stopCallBack">An optional action that is triggered when this session terminates.</param>
        public GameSession(DiscordSocketClient client, SocketMessage initialMessage, SessionInitParameters initParameters, Action<GameSession> stopCallBack = null) {
            this.client = client;
            this.initParameters = initParameters;
            this.stopCallBack = stopCallBack;
            sessionEvents = new SessionEventWrapper(client);
            owner = initialMessage.Author;
            Channel = initialMessage.Channel;
            gameSessionData = new GameSessionData(Channel);

            players.Add(initialMessage.Author);
            sessionEvents.MessageReceived += BaseMessageReceiveEvent;

            var sendMessageTask = initialMessage.Channel.SendMessageAsync(MessageRepository.GameInitMessage);
            //To ensure announcement message is sent.
            sendMessageTask.Wait();

            Logger.Info("*********************************************************");
            Logger.Info($"* Game session has been created by {initialMessage.Author} in channel: {initialMessage.Channel.Name}.");
            Logger.Info("*********************************************************");
        }

        public void StartGame() {
            if (IsRunning) {
                Logger.Warn("Can't start game because it's already running.");
                return;
            }

            IsRunning = true;
            gameSessionData.Players = characters;

            Logger.Debug($"Initializing game session in {Channel}");
            GatherPlayersAsync();
        }

        private async Task GatherPlayersAsync() {
            try {
                sessionEvents.MessageReceived += GatherMessageReceiveEvent;
                Logger.Debug("Waiting for players..");

                //wait for players
                await Task.Delay(initParameters.JoinWaitLength);
                Logger.Debug("Stopped waiting, checking players.");
                var minPlayers = initParameters.MinimumPlayers;
                if (players.Count < minPlayers) {
                    Logger.Debug($"Game has only {players.Count} players. Required at least {minPlayers}");
                    Channel.SendMessageAsync(string.Format(MessageRepository.GameTooFewPlayersToStart, players.Count, minPlayers));
                    StopGame();
                }
                else if(IsRunning){
                    Logger.Debug($"{players.Count} players joined. Assigning roles..");
                    AssignRolesAsync();
                }

                sessionEvents.MessageReceived -= GatherMessageReceiveEvent;
            }
            catch (Exception e) {
                Logger.Error($"Unexpected error while gathering players in {Channel}. {e.Message}", e);
                StopGame($"Unexpected exception! {e.Message}\nGame will abort!");
            }
        }

        private async Task AssignRolesAsync() {
            try {
                var unassignedUsers = new HashSet<SocketUser>(players);
                var random = new Random();

                //Select werewolves.
                int werewolvesNumber = (int) (players.Count * initParameters.WerewolfRatio + 1);
                if (unassignedUsers.Count >= werewolvesNumber) {
                    for (int i = 0; i < werewolvesNumber; i++) {
                        var newWerewolf = unassignedUsers.ElementAt(random.Next(unassignedUsers.Count));
                        characters.Add(new Character(Werewolf, newWerewolf));
                        unassignedUsers.Remove(newWerewolf);
                    }
                    Logger.Debug($"Assigned {werewolvesNumber} werewolves in {Channel}");
                }
                else {
                    Logger.Warn($"According to the configs there should be enough player for {werewolvesNumber} werewolves, but there aren't enough in {Channel}! skipping them.");
                }

                //Select warlocks
                int warlockNumber = (int) (players.Count * initParameters.WarlockRatio);
                if (random.Next(1, 100) < initParameters.AdditionalWarlockChance) {
                    warlockNumber++;
                }

                if (unassignedUsers.Count >= warlockNumber) {
                    for (int i = 0; i < warlockNumber; i++) {
                        var newWarlock = unassignedUsers.ElementAt(random.Next(unassignedUsers.Count));
                        characters.Add(new Character(Warlock, newWarlock));
                        unassignedUsers.Remove(newWarlock);
                    }
                    Logger.Debug($"Assigned {warlockNumber} warlocks in {Channel}");
                }
                else {
                    Logger.Warn($"According to the configs there should be enough player for {warlockNumber} warlocks, but there aren't enough in {Channel}! skipping them.");
                }

                //Select oracles
                int oracleNumber = (int) (players.Count * initParameters.OracleRatio);
                if (random.Next(1, 100) < initParameters.AdditionalOracleChance) {
                    oracleNumber++;
                }

                if (unassignedUsers.Count >= oracleNumber) {
                    for (int i = 0; i < oracleNumber; i++) {
                        var newOracle = unassignedUsers.ElementAt(random.Next(unassignedUsers.Count));
                        characters.Add(new Character(Oracle, newOracle));
                        unassignedUsers.Remove(newOracle);
                    }

                    Logger.Debug($"Assigned {oracleNumber} oracles in {Channel}");
                }
                else {
                    Logger.Warn($"According to the configs there should be enough player for {oracleNumber} oracles, but there aren't enough in {Channel}! skipping them.");
                }

                //Select drunks
                int drunkNumber = (int) (players.Count * initParameters.DrunkRatio);
                if (random.Next(1, 100) < initParameters.AdditionalDrunkChance) {
                    drunkNumber++;
                }

                if (unassignedUsers.Count >= drunkNumber) {
                    for (int i = 0; i < drunkNumber; i++) {
                        var newDrunk = unassignedUsers.ElementAt(random.Next(unassignedUsers.Count));
                        characters.Add(new Character(Drunk, newDrunk));
                        unassignedUsers.Remove(newDrunk);
                    }

                    Logger.Debug($"Assigned {drunkNumber} drunks in {Channel}");
                }
                else {
                    Logger.Warn($"According to the configs there should be enough player for {drunkNumber} oracles, but there aren't enough in {Channel}! skipping them.");
                }
                
                //Select guardian angels
                int gaNumber = (int) (players.Count * initParameters.GuardianAngelRatio);
                if (random.Next(1, 100) < initParameters.AdditionalGuardianAngelChance) {
                    gaNumber++;
                }

                if (unassignedUsers.Count >= gaNumber) {
                    for (int i = 0; i < gaNumber; i++) {
                        var newGA = unassignedUsers.ElementAt(random.Next(unassignedUsers.Count));
                        characters.Add(new Character(GuardianAngel, newGA));
                        unassignedUsers.Remove(newGA);
                    }

                    Logger.Debug($"Assigned {gaNumber} guardian angels in {Channel}");
                }
                else {
                    Logger.Warn($"According to the configs there should be enough player for {gaNumber} guardian angels, but there aren't in {Channel}! Skipping thew.");
                }
                
                //Select gunners
                int gunnerNumber = (int) (players.Count * initParameters.GunnerRatio);
                if (random.Next(1, 100) < initParameters.AdditionalGunnerChance) {
                    gunnerNumber++;
                }

                if (unassignedUsers.Count >= gunnerNumber) {
                    for (int i = 0; i < gunnerNumber; i++) {
                        var newGunner = unassignedUsers.ElementAt(random.Next(unassignedUsers.Count));
                        characters.Add(new GunnerCharacter(Gunner, newGunner));
                        unassignedUsers.Remove(newGunner);
                    }

                    Logger.Debug($"Assigned {gunnerNumber} gunners in {Channel}");
                }
                else {
                    Logger.Warn($"According to the configs there should be enough player for {gunnerNumber} gunners, but there aren't in {Channel}! skipping them.");
                }
                
                //Select Cursed
                int cursedNumber = (int) (players.Count * initParameters.CursedRatio);
                if (random.Next(1, 100) < initParameters.AdditionalCursedChance) {
                    cursedNumber++;
                }

                if (unassignedUsers.Count >= cursedNumber) {
                    for (int i = 0; i < cursedNumber; i++) {
                        var newCursed = unassignedUsers.ElementAt(random.Next(unassignedUsers.Count));
                        characters.Add(new Character(Cursed, newCursed));
                        unassignedUsers.Remove(newCursed);
                    }

                    Logger.Debug($"Assigned {cursedNumber} cursed in {Channel}");
                }
                else {
                    Logger.Warn($"According to the configs there should be enough player for {cursedNumber} cursed, but there aren't in {Channel}! skipping them.");
                }
                
                //Select Cupid
                if (random.Next(1, 100) < initParameters.CupidChance) {
                    if (unassignedUsers.Count >= 1) {
                        var newCupid = unassignedUsers.ElementAt(random.Next(unassignedUsers.Count));
                        characters.Add(new Character(Cupid, newCupid));
                        unassignedUsers.Remove(newCupid);
                    }
                    else {
                        Logger.Warn($"According to the configs there should be enough player for a cupid, but there isn't in {Channel}! skipping them.");
                    }
                }

                int MasochistNumber = (int) (players.Count * initParameters.MasochistRatio);
                if (random.Next(1, 100) < initParameters.AdditionalMasochistChance) {
                    MasochistNumber++;
                }

                if (unassignedUsers.Count >= MasochistNumber) {
                    for (int i = 0; i < MasochistNumber; i++) {
                        var newMasochist = unassignedUsers.PickRandom();
                        characters.Add(new Character(Character.Type.Masochist, newMasochist));
                        unassignedUsers.Remove(newMasochist);
                    }
                    
                    Logger.Debug($"Assigned {MasochistNumber} masochist in {Channel}");
                }
                else {
                    Logger.Warn($"According to the configs there should be enough player for {MasochistNumber} masochist, but there aren't in {Channel}! skipping them.");
                }
                

                //Everyone else is a villager
                if (unassignedUsers.Count == 0) {
                    Logger.Warn($"No players left for simple villagers in {Channel}");
                }
                foreach (var user in unassignedUsers) {
                    characters.Add(new Character(Villager, user));
                }

                Logger.Debug($"Roles are assigned in {Channel}");
                await AnnounceRolesAsync();

                //Wait a bit so people can read 
                await Task.Delay(initParameters.RoleAssignWaitLength);

                GameAsync();
            }
            catch (Exception e) {
                Logger.Error($"Unexpected error while assigning roles in {Channel}. {e.Message}", e);
                StopGame($"Unexpected exception! {e.Message}\nGame will abort!");
            }
        }

        private async Task AnnounceRolesAsync() {
            var messageTasks = new HashSet<Task<IUserMessage>>();
            //sends all messages, and doesn't wait for their return.
            foreach (var character in characters) {
                switch (character.CharacterType) {
                    case Villager:
                        messageTasks.Add(character.user.SendMessageAsync(MessageRepository.AnnounceRoleAssignedToVillager));
                        break;
                    case Werewolf:
                        string partners = "\nYour partners: ";
                        var otherWolves = characters.Where(other => other.CharacterType == Werewolf && !other.Equals(character)).ToList();
                        partners += otherWolves.Count > 0 ? string.Join(", ", otherWolves.Select(otherWolf => otherWolf.Name)) : "You are a lone wolf!";
                        messageTasks.Add(character.user.SendMessageAsync(MessageRepository.AnnounceRoleAssignedToWerewolf + partners));
                        break;
                    case Oracle:
                        messageTasks.Add(character.user.SendMessageAsync(MessageRepository.AnnounceRoleAssignedToOracle));
                        break;
                    case Drunk:
                        messageTasks.Add(character.user.SendMessageAsync(MessageRepository.AnnounceRoleAssignedToDrunk));
                        break;
                    case Gunner:
                        messageTasks.Add(character.user.SendMessageAsync(MessageRepository.AnnounceRoleAssignedToGunner));
                        break;
                    case Cursed:
                        messageTasks.Add(character.user.SendMessageAsync(MessageRepository.AnnounceRoleAssignedToCursed));
                        break;
                    case Cupid:
                        messageTasks.Add(character.user.SendMessageAsync(MessageRepository.AnnounceRoleAssignedToCupid));
                        break;
                    case GuardianAngel:
                        messageTasks.Add(character.user.SendMessageAsync(MessageRepository.AnnounceRoleAssignedToGuardianAngel));
                        break;
                    case Warlock:
                        messageTasks.Add(character.user.SendMessageAsync(MessageRepository.AnnounceRoleAssignedToWarlock));
                        break;
                    case Character.Type.Masochist:
                        messageTasks.Add(character.user.SendMessageAsync(MessageRepository.AnnounceRoleAssignedToMasochist));
                        break;
                }
            }

            //Now that we sent all messages, wait for them all. (They can be processed parallelly)
            foreach (var messageTask in messageTasks) {
                await messageTask;
            }
        }

        /// <summary>
        /// Main logic of the game. This Task runs throughout the whole duration of the game.
        /// </summary>
        private async Task GameAsync() {
            try {
                Logger.Debug($"Game is initialized and about to start in {Channel}");
                Channel.SendMessageAsync(MessageRepository.GameStartedAnnouncement);
                GameState gameState;
                bool werewolvesDrunk = false;
                RemakeGunnerMessages();
                
                while ((gameState = GetState()) == GameState.Running && IsRunning) {
                    Logger.Debug($"Starting day {day} in {Channel}.");
                    var nightResult = await NightAsync(werewolvesDrunk);
                    Logger.Debug($"Night ended in {Channel}. Results: {nightResult}");
                    werewolvesDrunk = false;

                    //processing night results
                    
                    //Handling cupid if there is one
                    if (nightResult.CupidVoteResult != null && nightResult.CupidVoteResult.Votes.Any()) {
                        var cupid = nightResult.CupidVoteResult.Votes.Keys.First();
                        var lovers = nightResult.CupidVoteResult.Votes[cupid];
                        if (lovers.Count == 2) {
                            Logger.Debug($"Set {lovers[0]} and {lovers[1]} as lovers in {Channel}.");
                            lovers[0].Lover = lovers[1];
                            lovers[1].Lover = lovers[0];
                            lovers[0].user.SendMessageAsync(string.Format(MessageRepository.LoveAnnouncementMessages, lovers[1].Name));
                            lovers[1].user.SendMessageAsync(string.Format(MessageRepository.LoveAnnouncementMessages, lovers[0].Name));
                            cupid.CharacterType = Villager;
                        }
                    }
                    
                    string morningMessage;
                    var werewolfVictim = nightResult.WerewolfVoteResult.Victim;
                    if (werewolfVictim != null) {
                        Logger.Debug($"Werewolves attacked {werewolfVictim} in {Channel}");
                        if (werewolfVictim.CharacterType == Cursed) {
                            Logger.Debug($"{werewolfVictim} is cursed and transforming to werewolf in {Channel}");
                            //Cursed transforms
                            werewolfVictim.user.SendMessageAsync(MessageRepository.CursedTransformMessage);
                            foreach (var werewolf in characters.Where(character => character.IsAlive && character.CharacterType == Werewolf)) {
                                werewolf.user.SendMessageAsync(string.Format(MessageRepository.CursedTransformAnnounceMessage, werewolfVictim.Name));
                            }
                            werewolfVictim.CharacterType = Werewolf;
                            
                            morningMessage = MessageRepository.NoWerewolfKillMessage;
                        } else if (nightResult.GuardianAngelResult.Votes.Values.SelectMany(list => list).Contains(werewolfVictim)) { 
                            //If a guardian angel saved the victim
                            Logger.Debug($"A guardian angel saved {werewolfVictim} in {Channel}");
                            morningMessage = MessageRepository.GuardianAngelSavedMessage;
                            
                        }
                        else {
                            //Kill victim.
                            KillCharacter(werewolfVictim);
                            morningMessage = string.Format(MessageRepository.WerewolfKillMessage, werewolfVictim.Name);
                            if (werewolfVictim.CharacterType == Drunk) {
                                werewolvesDrunk = true;
                                foreach (var werewolf in characters.Where(character => character.IsAlive && character.CharacterType == Werewolf)) {
                                    werewolf.user.SendMessageAsync(MessageRepository.WerewolfAteDrunkAnnounceMessage);
                                }
                            }
                            //If lover, kill as well.
                            if (werewolfVictim.Lover != null) {
                                morningMessage += "\n\n" + string.Format(MessageRepository.LoverSuicidesMessage, werewolfVictim.Lover.Name);
                            }
                        }
                    }
                    else {
                        morningMessage = MessageRepository.NoWerewolfKillMessage;
                    }

                    //Announcing result
                    await Channel.SendMessageAsync(morningMessage);

                    if ((gameState = GetState()) != GameState.Running || !IsRunning)
                        break;

                    //Talky time
                    await Task.Delay(initParameters.DayLength);

                    //Vote for hanging
                    var dayResult = await DayVoteAsync();

                    string endDayMessage;
                    var lynchedVictim = dayResult.VillagerLynchResult.Victim;
                    if (lynchedVictim != null) {
                        Logger.Debug($"Lynching {lynchedVictim} in {Channel}");
                        KillCharacter(lynchedVictim);
                        endDayMessage = string.Format(MessageRepository.VillagerLynchMessage, lynchedVictim.Name);

                        if (lynchedVictim.CharacterTeam == Character.Side.Masochist) {
                            // Overwriting the default lynch message and saving the character
                            masochistLynched = lynchedVictim;
                            endDayMessage = string.Format(MessageRepository.MasochistLynchMessage, lynchedVictim.Name);
                        }

                        //lover
                        if (lynchedVictim.Lover != null) {
                            endDayMessage += "\n" + string.Format(MessageRepository.LoverSuicidesMessage, lynchedVictim.Lover.Name);
                        }
                    }
                    else {
                        endDayMessage = MessageRepository.NoLynchMessage;
                    }

                    await Channel.SendMessageAsync(endDayMessage);
                    if ((gameState = GetState()) != GameState.Running) {
                        break;
                    }

                    Logger.Debug($"{day}. day has ended in {Channel}.");
                    day++;
                }

                Logger.Debug($"Game has ended with result: {gameState} in {Channel}.");
                switch (gameState) {
                    case GameState.Running:
                        Logger.Warn($"Game exited while it's still running in {Channel}!");
                        StopGame();
                        return;
                    case GameState.VillagersWin:
                        gameSessionData.SessionResult = GameSessionData.Result.VillagerWin;
                        await Channel.SendMessageAsync(FormatEndMessage(MessageRepository.VillagerWinMessage));
                        break;
                    case GameState.WerewolvesWin:
                        gameSessionData.SessionResult = GameSessionData.Result.WerewolfWin;
                        await Channel.SendMessageAsync(FormatEndMessage(MessageRepository.WerewolfWinMessage));
                        break;
                    case GameState.MasochistWins:
                        gameSessionData.SessionResult = GameSessionData.Result.MasochistWin;
                        await Channel.SendMessageAsync(FormatEndMessage(MessageRepository.MasochistWinMessage));
                        break;
                    default:
                        throw new ArgumentOutOfRangeException($"Not implemented state: {gameState}");
                }

                gameSessionData.Days = day;
                gameSessionData.Save();
                StopGame();
            }
            catch (Exception e) {
                Logger.Error($"Unexpected error while game was running in {Channel}. {e.Message}", e);
                StopGame($"Unexpected exception! {e.Message}\nGame will abort!");
            }
        }

        private string FormatEndMessage(string message) {
            return $"\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*\n{message}\n\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*\\*";
        }
        private async Task<NightResult> NightAsync(bool werewolvesDrunk) {
            try {
                Logger.Debug($"Starting night vote event in {Channel}");
                Logger.Debug($"Starting getting werewolf results in {Channel}");
                var werewolfVoteResultsTask = werewolvesDrunk ? Task<WerewolfVoteResult>.Factory.StartNew(() => new WerewolfVoteResult()) : GetWerewolfResultsAsync();
                Logger.Debug($"Starting handling oracles in {Channel}");
                var oracleObservationEventTask = OracleObservationEventAsync();
                var warlockObservationEventAsync = WarlockObservationEventAsync();

                //Is there a cupid? If not, Task should return null.
                Logger.Debug("Handling cupid");
                var cupid = characters.SingleOrDefault(character => character.CharacterType == Cupid);
                var cupidVoteResultTask = Task<CupidVoteResult>.Factory.StartNew(() => null);
                if (cupid != null) {
                    cupidVoteResultTask = GetCupidResultAsync(cupid);
                }
                
                Logger.Debug($"Starting getting ga results in {Channel}");
                var guardianAngelResultTask = GetGuardianAngelResultAsync();




                await oracleObservationEventTask;
                await warlockObservationEventAsync;
                Logger.Debug($"Awaiting night results in {Channel}");
                var nightResults = new NightResult(werewolfVoteResultsTask.Result, cupidVoteResultTask.Result, guardianAngelResultTask.Result);
                
                Logger.Debug($"Night events finished in {Channel}. Result: {nightResults}");
                return nightResults;
            }
            catch (Exception e) {
                Logger.Error($"Unexpected exception during night state of a game in {Channel}. {e.Message}", e);
                StopGame($"Unexpected exception! {e.Message}\nGame will abort!");
                throw;
            }
        }

        private async Task WarlockObservationEventAsync() {
            Logger.Debug($"Warlock conversation is started in {Channel}");
            var warlockReactionEvents = new ConcurrentDictionary<Character, ISet<Func<Cacheable<IUserMessage, ulong>, ISocketMessageChannel, SocketReaction, Task>>>();
            var warlockPMs = new ConcurrentBag<IUserMessage>();
            var warlockSelects = new ConcurrentDictionary<Character, List<Character>>();
            
            foreach (var character in characters) {
                if (character.CharacterType == Warlock && character.IsAlive) {
                    AddVoteReactionsAsync(
                        character,
                        characters.Where(otherCharacter => !Equals(character, otherCharacter) && otherCharacter.IsAlive).ToList(),
                        0,
                        MessageRepository.WarlockInspectMessage,
                        warlockReactionEvents,
                        warlockPMs,
                        warlockSelects,
                        1
                    );
                }
            }
            
            await Task.Delay(initParameters.NightVoteLength);

            var warlockPMEnumerator = warlockPMs.GetEnumerator();
            using (warlockPMEnumerator) {
                while (warlockPMEnumerator.MoveNext())
                    warlockPMEnumerator.Current.DeleteAsync();
            }
            
            foreach (var observer in warlockSelects.Keys) {
                if (warlockSelects[observer] != null && warlockSelects[observer].Count > 0) {
                    var observed = warlockSelects[observer][0];
                    switch (observed.CharacterType) {
                        case Villager:
                            observed.user.SendMessageAsync(MessageRepository.WarlockFindsVillagerMessages);
                            break;
                        case Werewolf:
                            observed.user.SendMessageAsync(MessageRepository.WarlockFindsWerewolfMessages);
                            break;
                        case Oracle:
                            observed.user.SendMessageAsync(MessageRepository.WarlockFindsOracleMessages);
                            break;
                        case Drunk:
                            observed.user.SendMessageAsync(MessageRepository.WarlockFindsDrunkMessages);
                            break;
                        case Gunner:
                            observed.user.SendMessageAsync(MessageRepository.WarlockFindsGunnerMessages);
                            break;
                        case Cursed:
                            observed.user.SendMessageAsync(MessageRepository.WarlockFindsCursedMessages);
                            break;
                        case Cupid:
                            observed.user.SendMessageAsync(MessageRepository.WarlockFindsCupidMessages);
                            break;
                        case GuardianAngel:
                            observed.user.SendMessageAsync(MessageRepository.WarlockFindsGuardianAngelMessages);
                            break;
                        case Warlock:
                            observed.user.SendMessageAsync(MessageRepository.WarlockFindsWarlockMessages);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException($"{observed.CharacterType} is not handled for warlock inspection");
                    }
                }
            }
            
        }

        private async Task OracleObservationEventAsync() {
            Logger.Debug($"Oracle conversation is started in {Channel}.");
            var oracleReactionEvents = new ConcurrentDictionary<Character, ISet<Func<Cacheable<IUserMessage, ulong>, ISocketMessageChannel, SocketReaction, Task>>>();
            var oraclePMs = new ConcurrentBag<IUserMessage>();
            var oracleSelects = new ConcurrentDictionary<Character, List<Character>>();

            foreach (var character in characters) {
                if (character.CharacterType == Oracle && character.IsAlive) {
                    AddVoteReactionsAsync(character,
                        characters.Where(otherCharacter => !Equals(character, otherCharacter) && otherCharacter.IsAlive).ToList(),
                        0,
                        MessageRepository.OracleInspectMessage,
                        oracleReactionEvents,
                        oraclePMs,
                        oracleSelects,
                        1);
                }
            }

            await Task.Delay(initParameters.NightVoteLength);

            var oraclePMEnumerator = oraclePMs.GetEnumerator();
            using (oraclePMEnumerator) {
                while (oraclePMEnumerator.MoveNext())
                    oraclePMEnumerator.Current.DeleteAsync();
            }

            foreach (Character observer in oracleSelects.Keys) {
                if (oracleSelects[observer] != null && oracleSelects[observer].Count > 0) {
                    Character observedCharacter = oracleSelects[observer][0];
                    switch (observedCharacter.CharacterType) {
                        case Villager:
                            observer.user.SendMessageAsync(string.Format(MessageRepository.OracleFindsVillagerMessages, observedCharacter.Name));
                            break;
                        case Werewolf:
                            observer.user.SendMessageAsync(string.Format(MessageRepository.OracleFindsWerewolfMessages, observedCharacter.Name));
                            break;
                        case Oracle:
                            observer.user.SendMessageAsync(string.Format(MessageRepository.OracleFindsOracleMessages, observedCharacter.Name));
                            break;
                        case Drunk:
                            observer.user.SendMessageAsync(string.Format(MessageRepository.OracleFindsDrunkMessages, observedCharacter.Name));
                            break;
                        case Gunner:
                            observer.user.SendMessageAsync(string.Format(MessageRepository.OracleFindsGunnerMessages, observedCharacter.Name));
                            break;
                        case Cursed:
                            observer.user.SendMessageAsync(string.Format(MessageRepository.OracleFindsCursedMessages, observedCharacter.Name));
                            break;
                        case Cupid:
                            observer.user.SendMessageAsync(string.Format(MessageRepository.OracleFindsCupidMessages, observedCharacter.Name));
                            break;
                        case GuardianAngel:
                            observer.user.SendMessageAsync(string.Format(MessageRepository.OracleFindsGuardianAngelMessages, observedCharacter.Name));
                            break;
                        case Warlock:
                            observer.user.SendMessageAsync(string.Format(MessageRepository.OracleFindsWarlockMessages, observedCharacter.Name));
                            break;
                        default:
                            throw new ArgumentOutOfRangeException($"{observedCharacter.CharacterType} is not handled for oracle inspection");
                    }
                }
            }
            Logger.Debug($"Oracle conversation is finished in {Channel}.");
        }

        /// <summary>
        /// Asks every individual werewolf in PMs for a vote of tonight's victim. <br/>
        /// Gathers all votes, and the result of the votes. 
        /// </summary>
        /// <returns>the result of the votes.</returns>
        private async Task<WerewolfVoteResult> GetWerewolfResultsAsync() {
            try {
                Logger.Debug($"Getting werewolf results in {Channel}..");
                var reactionEvents =
                    new ConcurrentDictionary<Character,
                        ISet<Func<Cacheable<IUserMessage, ulong>, ISocketMessageChannel, SocketReaction, Task>>>();
                var werewolvesPMs = new ConcurrentBag<IUserMessage>();
                var werewolfVotes = new ConcurrentDictionary<Character, List<Character>>();
                var werewolves = characters.Where(character => character.CharacterType == Werewolf && character.IsAlive)
                    .ToList();
                var targets = characters.Where(character => character.CharacterType != Werewolf && character.IsAlive)
                    .ToList();

                foreach (var werewolf in werewolves) {
                    string partners = "\n\n";
                    var otherWolves = characters.Where(other => other.CharacterType == Werewolf && other.IsAlive && !other.Equals(werewolf)).ToList();
                    partners += otherWolves.Count > 0 ? "Your partners: " + string.Join(", ", otherWolves.Select(character => character.Name)) : "You are a-lone wolf!";
                    AddVoteReactionsAsync(werewolf, targets, 0, MessageRepository.WerewolfNightVoteMessage + partners, reactionEvents, werewolvesPMs,
                        werewolfVotes, 1);
                }

                await Task.Delay(initParameters.NightVoteLength);

                //Clearing werewolf PMs and their reaction events.
                foreach (var pair in reactionEvents) {
                    foreach (var reactionEvent in pair.Value) {
                        sessionEvents.ReactionAdded -= reactionEvent;
                    }
                }

                var werewolfPMEnumerator = werewolvesPMs.GetEnumerator();
                using (werewolfPMEnumerator) {
                    while (werewolfPMEnumerator.MoveNext())
                        werewolfPMEnumerator.Current.DeleteAsync();
                }

                var werewolfVoteResult = new WerewolfVoteResult(werewolfVotes);
                Logger.Debug($"Werewolf vote result is created: {werewolfVoteResult}");
                return werewolfVoteResult;
            }
            catch (Exception e) {
                Logger.Error($"Unexpected exception during night state of a game in {Channel}. {e.Message}", e);
                StopGame($"Unexpected exception! {e.Message}\nGame will abort!");
                throw;
            }
        }

        private async Task<GuardianAngelResult> GetGuardianAngelResultAsync() {
            try {
                Logger.Debug($"Getting GA results in {Channel}");
                var guardianAngels = characters.Where(character => character.CharacterType == GuardianAngel && character.IsAlive).ToList();
                var reactionEvents =
                    new ConcurrentDictionary<Character,
                        ISet<Func<Cacheable<IUserMessage, ulong>, ISocketMessageChannel, SocketReaction, Task>>>();
                var gaPMs = new ConcurrentBag<IUserMessage>();
                var gaVotes = new ConcurrentDictionary<Character, List<Character>>();
                foreach (var guardianAngel in guardianAngels) {
                    AddVoteReactionsAsync(guardianAngel,
                        characters.Where(character => character.IsAlive && !Equals(character, guardianAngel)).ToList(),
                        0, MessageRepository.GuardianAngelVoteMessage, reactionEvents, gaPMs, gaVotes, 1);
                }

                await Task.Delay(PropertyContainer.GetInstance().GetIntPropertySafe(NightVoteLength));
            
                foreach (var pair in reactionEvents) {
                    foreach (var reactionEvent in pair.Value) {
                        sessionEvents.ReactionAdded -= reactionEvent;
                    }
                }

                var gaPMEnumerator = gaPMs.GetEnumerator();
                using (gaPMEnumerator) {
                    while (gaPMEnumerator.MoveNext()) {
                        gaPMEnumerator.Current.DeleteAsync();
                    }
                }
                
                var guardianAngelResult = new GuardianAngelResult {Votes = gaVotes};
                Logger.Debug($"Guardian angel vote result is created: {guardianAngelResult}");

                return guardianAngelResult;
            }
            catch (Exception e) {
                Logger.Error($"Unexpected exception during night state of a game in {Channel}. {e.Message}", e);
                StopGame($"Unexpected exception! {e.Message}\nGame will abort!");
                throw;
            }
        }

        private async Task<CupidVoteResult> GetCupidResultAsync(Character cupid) {
            try {
                Logger.Debug($"Getting cupid results in {Channel}..");
                if (cupid == null) {
                    return null;
                }
                var reactionEvents =
                    new ConcurrentDictionary<Character,
                        ISet<Func<Cacheable<IUserMessage, ulong>, ISocketMessageChannel, SocketReaction, Task>>>();
                var cupidPMs = new ConcurrentBag<IUserMessage>();
                var cupidVotes = new ConcurrentDictionary<Character, List<Character>>();
                //everyone who is not the cupid
                var others = characters.Where(character => !character.Equals(cupid) && character.IsAlive).ToList();
                AddVoteReactionsAsync(cupid, others, 0, MessageRepository.CupidVoteMessage, reactionEvents, cupidPMs, cupidVotes, 2);

                await Task.Delay(initParameters.NightVoteLength);
                
                foreach (var pair in reactionEvents) {
                    foreach (var func in pair.Value) {
                        sessionEvents.ReactionAdded -= func;
                    }
                }

                var cupidPMEnumerator = cupidPMs.GetEnumerator();
                using (cupidPMEnumerator) {
                    while (cupidPMEnumerator.MoveNext())
                        cupidPMEnumerator.Current.DeleteAsync();

                }

                var cupidVoteResult = new CupidVoteResult {Votes = cupidVotes};
                Logger.Debug($"Created cupid vote result: {cupidVoteResult} in {Channel}.");

                return cupidVoteResult;
            }
            catch (Exception e) {
                Logger.Error($"Unexpected exception during night state of a game in {Channel}. {e.Message}", e);
                StopGame($"Unexpected exception! {e.Message}\nGame will abort!");
                throw;
            }
        }

        private async Task<DayResult> DayVoteAsync() {
            try {
                Logger.Debug($"Initiating villager votes in {Channel}");

                RestUserMessage voteAnnouncingMessage = await Channel.SendMessageAsync(MessageRepository.AnnounceDayVotingMessage);

                var aliveCharacters = new List<Character>(characters.Where(character => character.IsAlive));
                //For later to remove them
                var reactionEvents = new ConcurrentDictionary<Character, ISet<Func<Cacheable<IUserMessage, ulong>, ISocketMessageChannel, SocketReaction, Task>>>();
                var votePMs = new ConcurrentBag<IUserMessage>();
                var votes = new ConcurrentDictionary<Character, List<Character>>();

                foreach (var aliveCharacter in aliveCharacters) {
                    //We can vote everyone but ourselves.
                    var votingCharacters = new List<Character>(aliveCharacters.Where(character => !character.Equals(aliveCharacter)));

                    AddVoteReactionsAsync(aliveCharacter, votingCharacters, 0, string.Format(MessageRepository.DayVoteDMessage), reactionEvents, votePMs, votes, 1, voteAnnouncingMessage);
                }

                await Task.Delay(initParameters.DayVoteLength);

                var villagerLynchResult = new VillagerLynchResult(votes);

                var dayResult = new DayResult {VillagerLynchResult = villagerLynchResult};

                foreach (var key in reactionEvents.Keys) {
                    foreach (var func in reactionEvents[key]) {
                        sessionEvents.ReactionAdded -= func;
                    }
                }

                foreach (var voteMessage in votePMs) {
                    voteMessage.DeleteAsync();
                }

                return dayResult;
            }
            catch (Exception e) {
                Logger.Error($"Unexpected exception during day state of a game in {Channel}. {e.Message}", e);
                StopGame($"Unexpected exception! {e.Message}\nGame will abort!");
                throw;
            }
        }

        /// <summary>
        /// Remakes all gunner PM dialog messages and their reaction events.
        /// Should be called whenever someone dies.
        /// </summary>
        private void RemakeGunnerMessages() {
            Logger.Debug($"Remaking gunner messages in {Channel}");
            //Deleting previous messages
            foreach (var gunnerResultMessage in gunnerShots.Messages) {
                gunnerResultMessage.DeleteAsync();
            }
            gunnerShots.Messages.Clear();
            //Removing reaction events.
            foreach (var gunnerResultGunnerReactionEvent in gunnerShots.GunnerReactionEvents) {
                foreach (var func in gunnerResultGunnerReactionEvent.Value) {
                    sessionEvents.ReactionAdded -= func;
                }
            }
            gunnerShots.GunnerReactionEvents.Clear();
            
            foreach (var gunnerResultTriggerReaction in gunnerShots.TriggerReactions) {
                sessionEvents.ReactionAdded -= gunnerResultTriggerReaction;
            }
            gunnerShots.TriggerReactions.Clear();
            

            foreach (var character in characters) {
                if (character.CharacterType == Gunner && character.IsAlive && ((GunnerCharacter) character).Bullets > 0) {
                    AddVoteReactionsAsync(character,
                        characters.Where((candidate) => !Equals(character, candidate) && candidate.IsAlive).ToList(),
                        0,
                        MessageRepository.GunnerVoteMessage,
                        gunnerShots.GunnerReactionEvents,
                        gunnerShots.Messages,
                        gunnerShots.Shots,
                        2);
                }
            }
            foreach (var gunnerResultTriggerReaction in gunnerShots.TriggerReactions) {
                sessionEvents.ReactionAdded += gunnerResultTriggerReaction;
            }
        }

        private async Task ShootPeople() {
            foreach (var (key, value) in gunnerShots.Shots) {
                foreach (var victim in value) {
                    if (victim.IsAlive) {
                        Logger.Debug($"{victim} is shot by {key} in {Channel}");
                        ((GunnerCharacter)key).Shoot();
                        KillCharacter(victim);
                        await Channel.SendMessageAsync(string.Format(MessageRepository.GunnerShotMessage,key.Name, victim.Name));
                        if (victim.Lover != null) {
                            Channel.SendMessageAsync(string.Format(MessageRepository.LoverSuicidesMessage, victim.Lover.Name));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// <p>Initializes a voting case. Sends a DM to a specific user,
        /// and gives them specific vote options through reaction emojis.</p>
        /// <p>This method has outgoing collection references where results are stored.
        /// You must wait for a fair amount of time before you can read from them to let voters make their choices.</p>
        /// </summary>
        /// <param name="votingCharacter">The target character we are requesting votes from.</param>
        /// <param name="candidates">A collection of characters that the target character can vote.</param>
        /// <param name="page">If there are more than 10 candidates, the message needs to paginate through them.
        ///                    This variable determinate the starting page.</param>
        /// <param name="messageBody">The body of the DM that is used to register the vote.
        ///                           This message will be appended by the list of the candidates.</param>
        /// <param name="reactionEvents">A reference of a dictionary that will contain all the reaction events of for the
        ///                              voting characters. Typically used clearing those events from the client after the voting ended.</param>
        /// <param name="targetUserPMs">A reference to a collection that will be appended by the PM message that is used for the voting</param>
        /// <param name="votes">A reference to a collection that is used to store the votes of characters.
        ///       This method creates reaction events that will append the list of this character in this dictionary with their votes</param>
        /// <param name="maxVotes">The maximum amount of candidates this character can vote on</param>
        private async Task AddVoteReactionsAsync(
            Character votingCharacter,
            IReadOnlyList<Character> candidates,
            int page,
            string messageBody,
            ConcurrentDictionary<Character,
                ISet<Func<Cacheable<IUserMessage, ulong>, ISocketMessageChannel, SocketReaction, Task>>> reactionEvents,
            ConcurrentBag<IUserMessage> targetUserPMs,
            ConcurrentDictionary<Character, List<Character>> votes,
            int maxVotes,
            RestUserMessage announceTo = null) {
            //Initializes reaction event collection if it's needed. If there are already reactions there, unregistering them.
            try {
                if (!reactionEvents.ContainsKey(votingCharacter)) {
                    reactionEvents[votingCharacter] =
                        new HashSet<Func<Cacheable<IUserMessage, ulong>, ISocketMessageChannel, SocketReaction, Task>>();
                }
                else {
                    foreach (var reactionEvent in reactionEvents[votingCharacter]) {
                        sessionEvents.ReactionAdded -= reactionEvent;
                    }
                }

                var dmChannel = await votingCharacter.user.GetOrCreateDMChannelAsync();
                //Building up the direct message's content.
                var messageText = messageBody;
                messageText += "\nCandidates:\n";
                for (var i = 0; i < candidates.Count; i++) {
                    if (page * 10 <= i && i < (page + 1) * 10) {
                        messageText += "\t" + EmojiUtils.DigitToEmoji(i + 1 - page * 10);
                    }
                    else {
                        messageText += "\t";
                    }
                    
                    messageText += $"{i + 1}: {candidates[i].user.Username}\n";
                }

                
                var targetDMessage = await dmChannel.SendMessageAsync(messageText);
                targetUserPMs.Add(targetDMessage);

                var rightEmoji = EmojiUtils.RightEmoji;
                var leftEmoji = EmojiUtils.LeftEmoji;

                //If needed, we show a left arrow first.
                if (page > 0) {
                    await targetDMessage.AddReactionAsync(leftEmoji);
                    var leftReactionEvent = client.OnSpecificReactionAdded((cacheable, channel, reaction) => {
                            targetDMessage.DeleteAsync().Start();
                            AddVoteReactionsAsync(votingCharacter, candidates, page - 1, messageBody, reactionEvents,
                                targetUserPMs, votes, maxVotes, announceTo).Start();
                        },
                        sessionEvents,
                        targetDMessage.Id,
                        leftEmoji,
                        votingCharacter.user.Id);
                    reactionEvents[votingCharacter].Add(leftReactionEvent);
                }
                
                if (candidates.Count < 10) {
                    for (var i = 0; (10 * page) + i < candidates.Count && i < 10; i++) {
                        await targetDMessage.AddReactionAsync(new Emoji(EmojiUtils.DigitToEmoji(i + 1)));
                        var reactionEvent = client.OnSpecificReactionAdded((cacheable, channel, reaction) => {
                                var votedCharacter = candidates[page * 10 + EmojiUtils.EmojiToDigit(reaction.Emote) - 1];
                                Logger.Debug($"{votingCharacter.user} clicked on a reaction option: {reaction.Emote.Name} to vote for {votedCharacter}.");
                                if (votes != null) {
                                    if (!votes.ContainsKey(votingCharacter))
                                        votes[votingCharacter] = new List<Character>();
                                    if (votes[votingCharacter].Count < maxVotes) {
                                        votes[votingCharacter].Add(votedCharacter);
                                        announceTo?.ModifyAsync(message => {
                                            announceTo.UpdateAsync().Wait();
                                            Logger.Debug("Editing message: " + announceTo.Content);
                                            message.Content = announceTo.Content + $"\n{votingCharacter.Name} voted: {votedCharacter.Name}";
                                        });
                                        
                                        if (votes[votingCharacter].Count == maxVotes) {
                                            if (targetDMessage is IUpdateable updateable) {
                                                updateable.UpdateAsync().Wait();
                                            }

                                            DiscordUtils.removeAllReactionsAsync(targetDMessage, client.CurrentUser).Start();
                                        }
                                    }
                                }
                                //Ugly hardcoded hack: if votes are the gunner vote list, trigger shoot event.
                                if (votes == gunnerShots.Shots) {
                                    ShootPeople();
                                }
                            },
                            sessionEvents,
                            targetDMessage.Id,
                            new Emoji(EmojiUtils.DigitToEmoji(i + 1)),
                            votingCharacter.user.Id);

                        reactionEvents[votingCharacter].Add(reactionEvent);
                    }
                }
                //If needed, we add a right arrow reaction
                if ((page + 1) * 10 < candidates.Count) {
                    targetDMessage.AddReactionAsync(rightEmoji);
                    var rightReactionEvent = client.OnSpecificReactionAdded((cacheable, channel, reaction) => {
                            Logger.Debug($"{votingCharacter.user} clicked on a reaction option: {reaction.Emote.Name}.");
                            targetDMessage.DeleteAsync().Start();
                            AddVoteReactionsAsync(votingCharacter, candidates, page + 1, messageBody, reactionEvents,
                                targetUserPMs, votes, maxVotes, announceTo).Start();
                        },
                        sessionEvents,
                        targetDMessage.Id,
                        rightEmoji,
                        votingCharacter.user.Id);
                    reactionEvents[votingCharacter].Add(rightReactionEvent);
                }
            }
            catch (Exception e) {
                Logger.Error($"Unexpected exception in AddReactions in {Channel}. {e.Message}", e);
                StopGame($"Unexpected exception! {e.Message}\nGame will abort!");
            }
        }

        private void KillCharacter(Character rip) {
            rip.IsAlive = false;
            //If lover, kill as well.
            if (rip.Lover != null) {
                rip.Lover.IsAlive = false; //Just to be sure. It shouldn't be needed since IsAlive's setter handles it.
            }
            
            if (gameSessionData.FirstKill == null)
                gameSessionData.FirstKill = rip;
            
            RemakeGunnerMessages();

        }

        /// <summary>
        /// Calculates the current state of the game. Used to determinate if there is already a winner of this game session.
        /// </summary>
        /// <returns>
        /// Returns <see cref="GameState.Running"/> if there is no winner team yet, or an enum for the winner team. 
        /// </returns>
        private GameState GetState() {
            //Winning conditions..
            if (masochistLynched != null)
                return GameState.MasochistWins;
            
            if (characters.Count(character => character.IsAlive && character.CharacterTeam == Werewolves)
                >= 
                characters.Count(character => character.IsAlive && character.CharacterTeam != Werewolves)) {
                return GameState.WerewolvesWin;
            }

            if (characters.Count(character => character.IsAlive && character.CharacterTeam == Werewolves) == 0)
                return GameState.VillagersWin;
            

            return GameState.Running;
        }

        private enum GameState {
            Running,
            VillagersWin,
            WerewolvesWin,
            MasochistWins
        }

        /// <summary>
        /// Stops this session. Drops all client events.
        /// </summary>
        /// <param name="errorMessage">Optional parameter for error messages.</param>
        internal void StopGame(string errorMessage = null) {
            try {
                IsRunning = false;
                if (errorMessage != null) {
                    Channel.SendMessageAsync(errorMessage);
                    Logger.Error($"Game session has stopped due to an internal error: {errorMessage}");
                }

                sessionEvents.Clear();
                stopCallBack?.Invoke(this);

                Logger.Info("*********************************************************");
                Logger.Info($"* Game session has stopped in channel: {Channel}");
                Logger.Info("*********************************************************");
            }
            catch (Exception e) {
                Logger.Warn($"Couldn't stop game in {Channel}", e);
            }
        }

        /// <summary>
        /// Message event for gathering players when the session is started .
        /// </summary>
        /// <param name="msg">The message that triggered this event.</param>
        private async Task GatherMessageReceiveEvent(SocketMessage msg) {
            var command = Command.ParseCommand(msg);
            if (command.IsCommand && command.Channel == Channel) {
                switch (command.CommandString) {
                    case "join":
                        //TODO: check if author is not playing in an other channel. Probably requires to replace this whole event handler to WerewolfBot.cs
                        if (!await DiscordUtils.IsDMOpen(command.Author, "You have joined the game")) {
                            Logger.Debug($"Can't send DM messages to {command.Author} therefore can't join the game in {Channel}");
                            Channel.SendMessageAsync($"{command.Author.Username} can't accept Direct messages. Please check your DM settings!");
                            return;
                        }

                        if (IsPlaying(command.Author)) {
                            Logger.Debug($"Join failed, {command.Author} has already joined.");
                            await Channel.SendMessageAsync($"{command.Author.Username} is already joined!");
                        }
                        else {
                            players.Add(command.Author);
                            Logger.Info($"{command.Author} has joined the game in {Channel}");
                            await Channel.SendMessageAsync($"{command.Author.Username} has joined the game.");
                        }

                        break;
                    case "leave":
                        if (IsPlaying(command.Author)) {
                            players.Remove(command.Author);
                            Logger.Info($"{command.Author} has left the game.");
                            await Channel.SendMessageAsync($"{command.Author.Username} has left the game. Coward!");
                        }
                        else {
                            Logger.Debug($"leave failed, {command.Author} has not joined.");
                            await Channel.SendMessageAsync(
                                $"{command.Author.Username}, you can't leave because you are not playing");
                        }

                        break;
                }
            }
        }

        private async Task BaseMessageReceiveEvent(SocketMessage msg) {
            var command = Command.ParseCommand(msg);
            if (command.IsCommand) {
                Logger.Debug($"Game session in channel {Channel} received a command: {msg.Content}");
                switch (command.CommandString.ToLower()) {
                    case "gamestatus":
                        Logger.Debug(
                            $"Game session is alive in {Channel}");
                        string statusMessage = $"Game is running.\nDay: {day}\n\nCharacters:\n";
                        foreach (var character in characters) {
                            statusMessage += $"{character.Name}: " + (character.IsAlive ? "alive\n" : "dead\n");
                        }
                        Channel.SendMessageAsync(statusMessage);
                        break;
                    case "stopgame":
                        if (owner == msg.Author) {
                            StopGame();
                            await msg.Channel.SendMessageAsync("Game is stopped!");
                        }
                        else {
                            await msg.Channel.SendMessageAsync("You are not the owner of this game!");
                        }

                        break;
                }
            }
        }

        protected bool Equals(GameSession other) {
            return Equals(Channel, other.Channel);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((GameSession) obj);
        }

        public override int GetHashCode() {
            return Channel != null ? Channel.GetHashCode() : 0;
        }

        public override string ToString() {
            return $"Game session in {Channel}. Game is running: {IsRunning}. Joined players: {players}";
        }
    }
}