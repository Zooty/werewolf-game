using System.Collections.Generic;
using System.Linq;
using Werewolf_game.WerewolfBot.Characters;

namespace Werewolf_game.WerewolfBot.Results {
    public class CupidVoteResult {
        public IReadOnlyDictionary<Character, List<Character>> Votes { get; set; }

        /// <summary>
        /// Since there should be maximum 1 cupid, we only return the first list.
        /// </summary>
        /// <returns>The list of votes of the cupid is there is one, or null if there isn't any cupid in the game.</returns>
        public List<Character> GetLovers() {
            return Votes.Values.Any() ? Votes.Values.First() : null;
        }
        
        public override string ToString() {
            return $"Votes: {Votes.Values}";
        }
    }
}