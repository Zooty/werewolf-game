using System;

namespace Werewolf_game.WerewolfBot.Results {
    internal class NightResult {
        internal WerewolfVoteResult WerewolfVoteResult { get; }
        
        internal CupidVoteResult CupidVoteResult { get; }

        public GuardianAngelResult GuardianAngelResult { get; }

        public NightResult(WerewolfVoteResult werewolfVoteResult, CupidVoteResult cupidVoteResult, GuardianAngelResult guardianAngelResult) {
            Console.Out.WriteLine("creating NightResult");
            WerewolfVoteResult = werewolfVoteResult;
            CupidVoteResult = cupidVoteResult;
            GuardianAngelResult = guardianAngelResult;
        }

        public override string ToString() {
            return $"{nameof(WerewolfVoteResult)}: [{WerewolfVoteResult}] " +
                   $"CupidVoteResult: [{CupidVoteResult}] " +
                   $"{nameof(GuardianAngelResult)}: [{GuardianAngelResult}]";
        }
    }
}