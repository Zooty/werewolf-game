using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using log4net;
using Werewolf_game.Extensions;
using Werewolf_game.WerewolfBot.Characters;

namespace Werewolf_game.WerewolfBot.Results {
     class VillagerLynchResult {
        private static readonly ILog Logger =
            LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

         public VillagerLynchResult(ConcurrentDictionary<Character, List<Character>> votes) {
             //Since villagers can only vote for one person, no need for a list.
             var singleVotes = new ConcurrentDictionary<Character, Character>();
             foreach (var votesKey in votes.Keys) {
                 singleVotes[votesKey] = votes[votesKey].Count > 0 ? votes[votesKey][0] : null;
             }

             Votes = singleVotes;
         }

         private ConcurrentDictionary<Character, Character> votes;

        public ConcurrentDictionary<Character, Character> Votes {
            get => votes;
            set {
                votes = value;
                if (Votes.Count > 0) {
                    var votedCharacters = Votes.Values.ToList();
                    var groupedVotes = votedCharacters
                        .GroupBy(character => character, (ch, count) => new {Character = ch, Count = count.Count()})
                        .ToList();
                    int maxVotes = groupedVotes.Max(val => val.Count);

                    Victim = groupedVotes.Where(vote => vote.Count == maxVotes).PickRandom().Character;
                    Logger.Debug($"The village has decided to lynch {Victim}.");
                }
                else {
                    Logger.Debug("No kills by the village.");
                }
            }
        }
        
        public Character Victim { get; private set; }
    }
}