using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using Werewolf_game.WerewolfBot.Characters;

namespace Werewolf_game.WerewolfBot.Results {
    public class GunnerResult {
        public  ConcurrentDictionary<Character,
            ISet<Func<Cacheable<IUserMessage, ulong>, ISocketMessageChannel, SocketReaction, Task>>> GunnerReactionEvents { get; } = new ConcurrentDictionary<Character, ISet<Func<Cacheable<IUserMessage, ulong>, ISocketMessageChannel, SocketReaction, Task>>>();

        public ConcurrentDictionary<Character, List<Character>> Shots { get; } = new ConcurrentDictionary<Character, List<Character>>();

        public ConcurrentBag<IUserMessage> Messages { get; } = new ConcurrentBag<IUserMessage>();

        public ISet<Func<Cacheable<IUserMessage, ulong>, ISocketMessageChannel, SocketReaction, Task>> TriggerReactions { get; } = new HashSet<Func<Cacheable<IUserMessage, ulong>, ISocketMessageChannel, SocketReaction, Task>>();
    }
}