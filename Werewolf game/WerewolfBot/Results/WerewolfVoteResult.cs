using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Discord;
using log4net;
using Werewolf_game.Extensions;
using Werewolf_game.WerewolfBot.Characters;

namespace Werewolf_game.WerewolfBot.Results {
    
    /// <summary>
    /// Wrapper class for all the results that is created in a night from the actions of the alive werewolves.
    /// </summary>
    internal class WerewolfVoteResult {
        private static readonly ILog Logger =
            LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        private IReadOnlyDictionary<Character, Character> votes;

        /// <summary>
        /// Creates an empty 'voteless' result.
        /// </summary>
        public WerewolfVoteResult() { }

        /// <summary>
        /// Creates results from the given votes.
        /// </summary>
        /// <param name="votes">votes of the werewolves.</param>
        public WerewolfVoteResult(ConcurrentDictionary<Character, List<Character>> votes) {
            //Since werewolves can only vote for one villagers, no need for a list.
            var singleVotes = new Dictionary<Character, Character>();
            foreach (var votesKey in votes.Keys) {
                singleVotes[votesKey] = votes[votesKey].Count > 0 ? votes[votesKey][0] : null;
                if (votes[votesKey].Count > 1) {
                    Logger.Warn($"{votesKey.user} has voted more than one person as a werewolf!");
                }
                if (votesKey.CharacterTeam != Character.Side.Werewolves) {
                    Logger.Warn($"{votesKey.user} has voted as a werewolf, but his role is {votesKey.CharacterType}!");
                }
            }

            Votes = singleVotes;
        }

        public IReadOnlyDictionary<Character, Character> Votes {
            get => votes;
            set {
                votes = value;
                if (Votes.Count > 0) {
                    var votedCharacters = Votes.Values.ToList();
                    var groupedVotes = votedCharacters
                        .GroupBy(character => character, (ch, count) => new {Character = ch, Count = count.Count()})
                        .ToList();
                    int maxVotes = groupedVotes.Max(val => val.Count);

                    Victim = groupedVotes.Where(vote => vote.Count == maxVotes).PickRandom().Character;
                    Logger.Debug($"Werewolves choose to kill {Victim}.");
                }
                else {
                    Logger.Debug("No kills by the werewolves.");
                }
            }
        }

        public Character Victim { get; private set; }

        public override string ToString() {
            return $"Votes: {Votes.Count}, {nameof(Victim)}: {Victim}";
        }
    }
}