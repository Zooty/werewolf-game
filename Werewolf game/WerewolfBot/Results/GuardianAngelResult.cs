using System.Collections.Generic;
using Werewolf_game.WerewolfBot.Characters;

namespace Werewolf_game.WerewolfBot.Results {
    public class GuardianAngelResult {
        public IReadOnlyDictionary<Character, List<Character>> Votes { get; set; }
        
        public override string ToString() {
            return $"Votes: {Votes.Values}";
        }
        
    }
}