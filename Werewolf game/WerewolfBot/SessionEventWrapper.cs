using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;

namespace Werewolf_game.WerewolfBot {
    public class SessionEventWrapper {
        private readonly DiscordSocketClient client;
        private readonly HashSet<Func<SocketMessage, Task>> messageReceivedEvents = new HashSet<Func<SocketMessage, Task>>();
        private readonly HashSet<Func<Cacheable<IUserMessage, ulong>, ISocketMessageChannel, SocketReaction, Task>> reactionAddedEvents = new HashSet<Func<Cacheable<IUserMessage, ulong>, ISocketMessageChannel, SocketReaction, Task>>();

        public SessionEventWrapper(DiscordSocketClient client) {
            this.client = client;
        }

        public event Func<SocketMessage, Task> MessageReceived
        {
            add
            {
                messageReceivedEvents.Add(value);
                client.MessageReceived += value;
            }
            remove
            {
                messageReceivedEvents.Remove(value);
                client.MessageReceived -= value;
            }
        }
        
        public event Func<Cacheable<IUserMessage, ulong>, ISocketMessageChannel, SocketReaction, Task> ReactionAdded
        {
            add
            {
                reactionAddedEvents.Add(value);
                client.ReactionAdded += value;
            }
            remove
            {
                reactionAddedEvents.Remove(value);
                client.ReactionAdded -= value;
            }
        }

        public void Clear() {
            foreach (var messageReceivedEvent in messageReceivedEvents) {
                client.MessageReceived -= messageReceivedEvent;
            }
            messageReceivedEvents.Clear();
            
            foreach (var reactionAddedEvent in reactionAddedEvents) {
                client.ReactionAdded -= reactionAddedEvent;
            }
            reactionAddedEvents.Clear();
        }
    }
}