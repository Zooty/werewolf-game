using System.Data;
using Discord.WebSocket;

namespace Werewolf_game.WerewolfBot.Characters {
    public class Character {
        private static readonly log4net.ILog Logger =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        private Type characterType;
        private bool isAlive;

        public bool IsAlive {
            get => isAlive;
            set {
                isAlive = value;
                //If this person had a lover, we kill that character as well.
                if (!isAlive && Lover != null && Lover.isAlive) {
                    Lover.IsAlive = false;
                }
            }
        }

        public Type CharacterType {
            get => characterType;
            set {
                characterType = value;
                switch (characterType) {
                    //Assigning characters to their teams
                    case Type.Werewolf:
                    case Type.Warlock:
                        CharacterTeam = Side.Werewolves;
                        break;
                    case Type.Villager:
                    case Type.Oracle:
                    case Type.Drunk:
                    case Type.Cupid:
                    case Type.Gunner:
                    case Type.Cursed:
                    case Type.GuardianAngel:
                        CharacterTeam = Side.Villagers;
                        break;
                    case Type.Masochist:
                        CharacterTeam = Side.Masochist;
                        break;
                }
            }
        }

        public bool IsWinner { get; set; } = false;

        public SocketUser user { get; }
        public Side CharacterTeam { get; private set; }
        public string Name { get; }
        public Character Lover { get; set; } = null;

        public Character(Type characterType, SocketUser user) {
            CharacterType = characterType;
            this.user = user ?? throw new NoNullAllowedException();
            this.Name = user.Username;

            IsAlive = true;
        }

        public enum Type {
            Villager,
            Werewolf,
            Oracle,
            Drunk,
            Gunner,
            Cursed,
            Cupid,
            GuardianAngel,
            Warlock,
            Masochist
        }
        
        public enum Side {
            Villagers,
            Werewolves,
            Masochist
        }

        protected bool Equals(Character other) {
            return Equals(user, other.user);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Character) obj);
        }

        public override int GetHashCode() {
            return (user != null ? user.GetHashCode() : 0);
        }

        public override string ToString() {
            return $"Character: [{nameof(Name)}: {Name}, {nameof(IsAlive)}: {IsAlive}, {nameof(CharacterType)}: {CharacterType}]";
        }
    }
}