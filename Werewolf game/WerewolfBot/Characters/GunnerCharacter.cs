using System;
using Discord.WebSocket;

namespace Werewolf_game.WerewolfBot.Characters {
    public class GunnerCharacter : Character {
        private static readonly log4net.ILog Logger =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        public int Bullets { get; private set; }

        public void Shoot() {
            if (Bullets > 0) {
                Bullets--;
            }
            else {
                Logger.Warn($"{user} tried to shoot with no bullets!");
            }
        }
        
        public GunnerCharacter(Type characterType, SocketUser user) : base(characterType, user) {
            if (characterType != Type.Gunner) {
                throw new ArgumentException("This class only allow Gunner types.");
            }

            Bullets = 2;
        }
    }
}