using System;
using System.Reflection;
using Configurations;
using Werewolf_game.Extensions;

namespace Werewolf_game.WerewolfBot {
	/// <summary>
	/// Wrapper class for initial parameters for a session.
	/// </summary>
	public class SessionInitParameters {
		private static readonly log4net.ILog Logger =
			log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		public int MinimumPlayers { get; set; }
		public int RoleAssignWaitLength { get; set; }
		public int JoinWaitLength { get; set; }
		public int DayLength { get; set; }
		public int DayVoteLength { get; set; }
		public int NightVoteLength { get; set; }

		public double WerewolfRatio { get; set; }
		public double WarlockRatio { get; set; }
		public int AdditionalWarlockChance { get; set; }
		public double OracleRatio { get; set; }
		public int AdditionalOracleChance { get; set; }
		public double DrunkRatio { get; set; }
		public int AdditionalDrunkChance { get; set; }
		public double GunnerRatio { get; set; }
		public int AdditionalGunnerChance { get; set; }
		public int CupidChance { get; set; }
		public double CursedRatio { get; set; }
		public int AdditionalCursedChance { get; set; }
		public double GuardianAngelRatio { get; set; }
		public int AdditionalGuardianAngelChance { get; set; }
		public double MasochistRatio { get; set; }
		public int AdditionalMasochistChance { get; set; }

		/// <summary>
		/// Initializes all properties from the config file. If it can't be parsed, sets a default value. 
		/// </summary>
		public SessionInitParameters() {
			var props = PropertyContainer.GetInstance();
			MinimumPlayers = props.GetIntPropertySafe(PropertyContainer.PropertyKey.MinPlayers, 8);
			RoleAssignWaitLength = props.GetIntPropertySafe(PropertyContainer.PropertyKey.RoleAssignWaitLength, 60000);
			JoinWaitLength = props.GetIntPropertySafe(PropertyContainer.PropertyKey.JoinWaitLength, 60000);
			DayLength = props.GetIntPropertySafe(PropertyContainer.PropertyKey.DayLength, 60000);
			DayVoteLength = props.GetIntPropertySafe(PropertyContainer.PropertyKey.DayVoteLength, 60000);
			NightVoteLength = props.GetIntPropertySafe(PropertyContainer.PropertyKey.NightVoteLength, 60000);

			WerewolfRatio = props.GetDoublePropertySafe(PropertyContainer.PropertyKey.WerewolfRatio, 0.10);
			WarlockRatio = props.GetDoublePropertySafe(PropertyContainer.PropertyKey.WarlockRatio);
			AdditionalWarlockChance = props.GetIntPropertySafe(PropertyContainer.PropertyKey.AdditionalWarlockChance);
			OracleRatio = props.GetDoublePropertySafe(PropertyContainer.PropertyKey.OracleRatio);
			AdditionalOracleChance = props.GetIntPropertySafe(PropertyContainer.PropertyKey.AdditionalOracleChance);
			DrunkRatio = props.GetDoublePropertySafe(PropertyContainer.PropertyKey.DrunkRatio);
			AdditionalDrunkChance = props.GetIntPropertySafe(PropertyContainer.PropertyKey.AdditionalDrunkChance);
			GunnerRatio = props.GetDoublePropertySafe(PropertyContainer.PropertyKey.GunnerRatio);
			AdditionalGunnerChance = props.GetIntPropertySafe(PropertyContainer.PropertyKey.AdditionalGunnerChance);
			CupidChance = props.GetIntPropertySafe(PropertyContainer.PropertyKey.CupidChance);
			CursedRatio = props.GetDoublePropertySafe(PropertyContainer.PropertyKey.CursedRatio);
			AdditionalCursedChance = props.GetIntPropertySafe(PropertyContainer.PropertyKey.AdditionalCursedChance);
			GuardianAngelRatio = props.GetDoublePropertySafe(PropertyContainer.PropertyKey.GuardianAngelRatio);
			AdditionalGuardianAngelChance = props.GetIntPropertySafe(PropertyContainer.PropertyKey.AdditionalGuardianAngelChance);
			MasochistRatio = props.GetDoublePropertySafe(PropertyContainer.PropertyKey.MasochistRatio);
			AdditionalMasochistChance = props.GetIntPropertySafe(PropertyContainer.PropertyKey.AdditionalMasochistChance);
		}

		/// <summary>
		/// Sets this object's parameters based on a given list of user arguments. 
		/// </summary>
		/// <param name="commandArguments"> The list of arguments.</param>
		public void ParseSettings(string[] commandArguments) {
			if (commandArguments != null) {
				for (var i = commandArguments.Length - 2; i >= 0; i--) {
					try {
						PropertyContainer.PropertyKey? foundProperty = null;
						foreach (var propertyEnum in (PropertyContainer.PropertyKey[]) Enum.GetValues(typeof(PropertyContainer.PropertyKey))) {
							if (propertyEnum.GetValue().ToLower() == "werewolf.game." + commandArguments[i].ToLower()) {
								foundProperty = propertyEnum;
								break;
							}
						}

						if (foundProperty != null && i != commandArguments.Length - 1) {
							var propertyName = Enum.GetName(typeof(PropertyContainer.PropertyKey), foundProperty);
							var valueString = commandArguments[i + 1];

							var propertyInfo = this.GetType().GetProperty(propertyName, BindingFlags.Public | BindingFlags.Instance);
							if (propertyInfo != null && propertyInfo.CanWrite) {
								if (propertyInfo.PropertyType == typeof(int)) {
									propertyInfo.SetValue(this, int.Parse(valueString));
								}
								else if (propertyInfo.PropertyType == typeof(double)) {
									propertyInfo.SetValue(this, double.Parse(valueString));
								}
							}
						}
					}
					catch (Exception e) {
						Logger.Warn("Exception during parsing a user property for a game session!", e);
					}
				}
			}
		}
		
		public override string ToString() => $"{nameof(MinimumPlayers)}: {MinimumPlayers}, {nameof(RoleAssignWaitLength)}: {RoleAssignWaitLength}, {nameof(JoinWaitLength)}: {JoinWaitLength}, {nameof(DayLength)}: {DayLength}, {nameof(DayVoteLength)}: {DayVoteLength}, {nameof(NightVoteLength)}: {NightVoteLength}, {nameof(WerewolfRatio)}: {WerewolfRatio}, {nameof(WarlockRatio)}: {WarlockRatio}, {nameof(AdditionalWarlockChance)}: {AdditionalWarlockChance}, {nameof(OracleRatio)}: {OracleRatio}, {nameof(AdditionalOracleChance)}: {AdditionalOracleChance}, {nameof(DrunkRatio)}: {DrunkRatio}, {nameof(AdditionalDrunkChance)}: {AdditionalDrunkChance}, {nameof(GunnerRatio)}: {GunnerRatio}, {nameof(AdditionalGunnerChance)}: {AdditionalGunnerChance}, {nameof(CupidChance)}: {CupidChance}, {nameof(CursedRatio)}: {CursedRatio}, {nameof(AdditionalCursedChance)}: {AdditionalCursedChance}, {nameof(GuardianAngelRatio)}: {GuardianAngelRatio}, {nameof(AdditionalGuardianAngelChance)}: {AdditionalGuardianAngelChance}, {nameof(MasochistRatio)}: {MasochistRatio}, {nameof(AdditionalMasochistChance)}: {AdditionalMasochistChance}";
	}
}