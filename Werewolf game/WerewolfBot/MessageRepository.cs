using System.Collections.Generic;
using Configurations;
using Werewolf_game.Extensions;
using Werewolf_game.Utils;

namespace Werewolf_game.WerewolfBot {
	//Maybe move to database? (Would allow multi language support)

	/// <summary>
	///    Static storage class for all the message templates the bot uses.
	/// </summary>
	internal static class MessageRepository {
		#region Lists

		private static readonly List<string> gameStartedAnnouncement = new List<string> {
			"TODO: Game started message.\n",
		};

		private static readonly List<string> announceRoleAssignedToVillager = new List<string> {
			"A beautiful village a peaceful life. You live here as a common **villager**, not worrying about anything.\n" +
			"You are happy, you are cheerful, you know your future must be bright and successful..\n\n" +
			"You have no idea how wrong you were..\n",
		};

		private static readonly List<string> announceRoleAssignedToWerewolf = new List<string> {
			"You hide a terrible secret. A curse that changed your life forever.\n" +
			"Years ago you have been bitten by a demonic beast: a **werewolf**. " +
			"Now you became one of them. Every night you transform into a vicious beast. " +
			"Your body is altered just as your mind. " +
			"You can only focus on one thing: to kill your next prey!\n\n" +
			"Every night you may kill a villager with your fellow werewolves. At day, you need to do your best " +
			"to keep your cover up. Your goal is to kill every non werewolf player in the village.\n",
		};

		private static readonly List<string> announceRoleAssignedToOracle = new List<string> {
			"You were born with a strange gift. You are clairvoyant! You see things others may not, and you are able to use this gift as you " +
			"wish. And you obviously use it to dig through other's dirty secrets.\n\n" +
			"You are an **oracle**.Every night you may choose a character to check their roles.\n",
		};

		private static readonly List<string> announceRoleAssignedToDrunk = new List<string> {
			"Life is unfair. Especially with you. Working hard every day drained your will to live, all your motivation almost completely. " +
			"Especially since all your hard work does not pay off. You are deeper and deeper in debt. But worry not, you found a way out!\n" +
			"It's ALCOHOL!\n\n" +
			"You are the **village drunk** - never sober, always stoned. " +
			"You have enough alcohol in your body to knock out an entire werewolf pack for a day!\n",
		};

		private static readonly List<string> announceRoleAssignedToCupid = new List<string> {
			"Love is in the air! It's beautiful, it's passionate, and it is all in your hands.\n" +
			"Others just call you **cupid** for your ways to manipulate people's desire so well.\n\n" +
			"You may choose two people in the village (excluding yourself). They will madly fall in love with each other! " +
			"If one of them wins, the other person will win too. If one dies, the other follows as well.\n",
		};

		private static readonly List<string> announceRoleAssignedToGunner = new List<string> {
			"You live a peaceful life in this village. But only since you retired. " +
			"Before that, you were a famous **gunslinger** who fought countless creatures of darkness.\n" +
			"But that's your past that you left behind. You only kept your bad memories and your scars from that life.\n" +
			"Oh, and your gun! But only with two silver bullets.\n\n" +
			"Anytime in the game you may shoot a person, instantly killing them. " +
			"This act will revealed for everyone, so be careful. Remember, you only have two bullets!\n",
		};

		private static readonly List<string> announceRoleAssignedToCursed = new List<string> {
			"You live your boring life in your village. " +
			"You wish something interesting would happen to you, but that's never the case. The most notable thing of today " +
			"is an old mad woman **cursing** you for not greeting her politely. It was something about unspeakable pain and dreadful werewolves. " +
			"What a crazy old woman! You shrug it off as if nothing happened. Not a wise decision...\n",
		};

		private static readonly List<string> announceRoleAssignedToGuardianAngel = new List<string> {
			"Ever since you were a child, you always tried your best to be as helpful as possible." +
			"You heal the wounded, protect the weak, save the innocents. But only if you get there in time!\n" +
			"You are the **guardian angel** of the village, even though your work is never recognized and people " +
			"almost always forget all of your hard work. But that never stopped you from doing good!\n\n" +
			"Every night you may choose one villager to protect from harm.\n",
		};

		private static readonly List<string> announceRoleAssignedToWarlock = new List<string> {
			"TODO: U a warlock"
		};

		private static readonly List<string> announceRoleAssignedToMasochist = new List<string> {
			"TODO: you a kinky fucker (masochist)"
		};

		private static readonly List<string> werewolfNightVoteMessage = new List<string> {
			"The day has ended. The chill of the night and the moonlight on your fur " +
			"triggers your instincts. It's time to hunt! You just need to decide your next victim.\n",
		};

		private static readonly List<string> cupidVoteMessage = new List<string> {
			"It's time to alter some fates!\n" +
			"Choose two characters that will madly fall in love with each other!\n",
		};

		private static readonly List<string> guardianAngelVoteMessage = new List<string> {
			"TODO: GA vote message\n",
		};

		private static readonly List<string> gunnerVoteMessage = new List<string> {
			"TODO: shoot!\n",
		};

		private static readonly List<string> werewolfKillMessages = new List<string> {
			"All the Villagers gather around a house this morning. " +
			"You can tell from their dreadful face that something horrible happened there. And you are right!\n" +
			"Blood and bodyparts everywhere! You find the remainings of {0}, who was brutally murdered by a vicious beast! " +
			"It is among you, and you must destroy it before it's too late.\n",

			"You wake up for a loud, horrified scream. As you run to the scene, you notice the terrible smell of the dead. " +
			"You enter the source of the sound: the house of {0}. The bloody room and the badly mauled body of {0} almost made you faint.\n" +
			"No doubt, there is a werewolf on the loose! The village collectively swears revenge!\n"
		};

		private static readonly List<string> cursedTransformMessage = new List<string> {
			"TODO:U have been bitten. U a wolf now. owo\n",
		};

		private static readonly List<string> cursedTransformAnnounceMessage = new List<string> {
			"TODO:{0} have been bitten. {0} is a wolf now. owo\n",
		};

		private static readonly List<string> werewolfAteDrunkAnnounceMessage = new List<string> {
			"TODO: You ate tons of alcohol. RIP u tomorrow!\n",
		};

		private static readonly List<string> gunnerShotMessages = new List<string> {
			"BOOM! The loud shot of a gun can be heard from anywhere in the village. But where is it coming from? " +
			"You look around and see {0} with a gun in his hand. The gun is aiming at {1} who just collapsed from the silver bullet of the gunslinger. " +
			"You'd better not get in trouble with this guy..\n",
		};

		private static readonly List<string> loverSuicidesMessage = new List<string> {
			"TODO: Oh no, my love dieded! {0} dies as well!\n",
			"TODO: Oh no, my love dieded! {0} dies as well!\n",
		};

		private static readonly List<string> villagerLynchMessages = new List<string> {
			"The village has decided! {0} must be the beast who needs to be taken down. " +
			"Without hesitation, the enraged villagers tied {0} to a pillar and started to bring firewood beneath him. " +
			"'Just like the good old days, we burn the wicked' they said. And that's what they did. The painful screams " +
			"could be heard from miles away as the flesh burnt down from {0}'s body. Then the screams stopped, and the " +
			"cheerful celebration of the village took its place! 'We are saved!' they yelled.\n" +
			"Were they right? Or did they just kill their own? We shall see soon.\n",
		};
		
		private static readonly List<string> masochistLynchMessage = new List<string> {
			"TODO: {0}: \"harder daddy!\" Masochist got lynched!"
		};

		private static readonly List<string> noWerewolfKillMessage = new List<string> {
			"You wake up to a beautiful day. The entire village acknowledges that you all " +
			"survived the night! However you all are sure about that the threat is not gone..\n"
		};

		private static readonly List<string> noLynchMessage = new List<string> {
			"TODO: peace, yo!\n",
			"TODO: peace, yo!\n",
		};

		private static readonly List<string> guardianAngelSavedMessage = new List<string> {
			"TODO: GA saved someone\n"
		};

		/// <summary>
		///    Announcement PM message to the character in love. Notifies the character about the partner's name. <br/>
		///    Needs to be formatted with <see cref="string.Format(string, object[])"/>with the following parameters:
		/// <list type="number">
		///     <item>The name of the loved person</item>
		/// </list>
		/// </summary>
		private static readonly List<string> loveAnnouncementMessages = new List<string> {
			"TODO1: U luv {0}\n",
			"TODO2: U luv {0}\n"
		};

		private static readonly List<string> oracleInspectMessage = new List<string> {
			"Finally the day is over. You can go away from the crowd and live to your hobby: " +
			"which is to learn other villagers dirty secrets. " +
			"Now you just need to decide whose you are going to check!\n"
		};

		private static readonly List<string> warlockInspectMessage = new List<string> {
			"TODO: Go check someone!",
		};

		private static readonly List<string> oracleFindsVillagerMessages = new List<string> {
			"TODO1: {0} is a boring villager\n",
			"TODO2: {0} is a boring villager\n"
		};

		private static readonly List<string> oracleFindsWerewolfMessages = new List<string> {
			"TODO1: {0} is a wolf!\n",
			"TODO2: {0} is a wolf!\n"
		};

		private static readonly List<string> oracleFindsDrunkMessages = new List<string> {
			"TODO1: {0} is a drunk ass\n",
			"TODO2: {0} is a drunk ass\n"
		};

		private static readonly List<string> oracleFindsOracleMessages = new List<string> {
			"You look into your crystal ball, and you see {0} looking back confused. " +
			"You see your colleague, a fellow oracle. You kinda feel dumb now.",
			"You look into your crystal ball, and you see {0} looking back confused. " +
			"You see your old nemesis. Another oracle, who always proved that you are worse in every way.\n" +
			"You are pretty pissed about it.\n"
		};

		private static readonly List<string> oracleFindsGunnerMessages = new List<string> {
			"TODO1: {0} is a gunner\n",
		};

		private static readonly List<string> oracleFindsCursedMessages = new List<string> {
			"TODO1: {0} is a cursed\n",
		};

		private static readonly List<string> oracleFindsCupidMessages = new List<string> {
			"TODO1: {0} is a Cupid\n",
		};

		private static readonly List<string> oracleFindsGuardianAngelMessages = new List<string> {
			"TODO1: {0} is a GA\n",
		};

		private static readonly List<string> oracleFindsWarlockMessages = new List<string> {
			"TODO1: {0} is a warlock\n",
		};

		private static readonly List<string> warlockFindsVillagerMessages = new List<string> {
			"TODO1: {0} is a boring villager\n",
			"TODO2: {0} is a boring villager\n"
		};

		private static readonly List<string> warlockFindsWerewolfMessages = new List<string> {
			"TODO1: {0} is a wolf!\n",
			"TODO2: {0} is a wolf!\n"
		};

		private static readonly List<string> warlockFindsDrunkMessages = new List<string> {
			"TODO1: {0} is a drunk ass\n",
			"TODO2: {0} is a drunk ass\n"
		};

		private static readonly List<string> warlockFindsOracleMessages = new List<string> {
			"TODO: {0} is an oracle.\n"
		};

		private static readonly List<string> warlockFindsGunnerMessages = new List<string> {
			"TODO1: {0} is a gunner\n",
		};

		private static readonly List<string> warlockFindsCursedMessages = new List<string> {
			"TODO1: {0} is a cursed\n",
		};

		private static readonly List<string> warlockFindsCupidMessages = new List<string> {
			"TODO1: {0} is a Cupid\n",
		};

		private static readonly List<string> warlockFindsGuardianAngelMessages = new List<string> {
			"TODO1: {0} is a GA\n",
		};

		private static readonly List<string> warlockFindsWarlockMessages = new List<string> {
			"TODO1: {0} is a warlock\n",
		};

		private static readonly List<string> announceDayVotingMessage = new List<string> {
			"TODO, vote plebs!\n"
		};

		private static readonly List<string> dayVoteDMessage = new List<string> {
			"TODO: Go kill ur neighbour!\n"
		};

		private static readonly List<string> werewolfWinMessage = new List<string> {
			"TODO Awoooo, we won ^w^!",
		};

		private static readonly List<string> villagerWinMessage = new List<string> {
			"TODO Plebs win!",
		};
		
		private static readonly List<string> masochistWinMessage = new List<string> {
			"TODO Masochist wins!"
		};

		#endregion

		#region Message constants

		/// <summary>
		///    The message the bot sends whenever a game is started by the StartGame command
		/// </summary>
		public static readonly string GameInitMessage = "Werewolf game has been started. Join by " +
		                                                PropertyContainer.GetInstance().GetProperty(PropertyContainer.PropertyKey.CommandPrefix) +
		                                                "join command!\n";

		/// <summary>
		///    The announcement message base when the game is unable to start due to too few players joined the session. <br/>
		///    Needs to be formatted with <see cref="string.Format(string, object[])"/>with the following parameters:
		/// <list type="number">
		///     <item>The amount of players joined</item>
		///     <item>The required amount of players to join a game session</item>
		/// </list>
		/// </summary>
		public const string GameTooFewPlayersToStart = "Only {0} players joined. Game requires at least {1} players.\n";

		/// <summary>
		///    Announcement for the very first day that initiates the game and introduces the story for the players.
		/// </summary>
		public static string GameStartedAnnouncement => gameStartedAnnouncement.PickRandom();

		/// <summary>
		///    Announcement PM to villagers
		/// </summary>
		public static string AnnounceRoleAssignedToVillager => announceRoleAssignedToVillager.PickRandom();

		/// <summary>
		///    Announcement PM to werewolves
		/// </summary>
		public static string AnnounceRoleAssignedToWerewolf => announceRoleAssignedToWerewolf.PickRandom();

		/// <summary>
		///    Announcement PM to oracles
		/// </summary>
		public static string AnnounceRoleAssignedToOracle => announceRoleAssignedToOracle.PickRandom();

		/// <summary>
		///    Announcement PM to drunk people
		/// </summary>
		public static string AnnounceRoleAssignedToDrunk => announceRoleAssignedToDrunk.PickRandom();

		/// <summary>
		///    Announcement PM to cupid
		/// </summary>
		public static string AnnounceRoleAssignedToCupid => announceRoleAssignedToCupid.PickRandom();

		/// <summary>
		///    Announcement PM to gunner
		/// </summary>
		public static string AnnounceRoleAssignedToGunner => announceRoleAssignedToGunner.PickRandom();

		/// <summary>
		///    Announcement PM to cursed
		/// </summary>
		public static string AnnounceRoleAssignedToCursed => announceRoleAssignedToCursed.PickRandom();

		/// <summary>
		///    Announcement PM to guardian angels
		/// </summary>
		public static string AnnounceRoleAssignedToGuardianAngel => announceRoleAssignedToGuardianAngel.PickRandom();

		/// <summary>
		///    Announcement PM to warlocks
		/// </summary>
		public static string AnnounceRoleAssignedToWarlock => announceRoleAssignedToWarlock.PickRandom();

		/// <summary>
		///    Announcement PM to masochists
		/// </summary>
		public static string AnnounceRoleAssignedToMasochist => announceRoleAssignedToMasochist.PickRandom();

		/// <summary>
		///    Announcement PM to a character that falls in love with someone else.
		///    Needs to be formatted with <see cref="string.Format(string, object[])"/>with the following parameters: <br/>
		/// <list type="number">
		///    <item>Name of the character that the target of the PM started to love</item>
		/// </list> 
		/// </summary>
		public static string LoveAnnouncementMessages => loveAnnouncementMessages.PickRandom();

		/// <summary>
		///    Announcement of a lover who suicides because the loved partner died. <br/>
		///    Needs to be formatted with <see cref="string.Format(string, object[])"/>with the following parameters: <br/>
		/// <list type="number">
		///    <item>Name of the lover who suicides</item>
		/// </list> 
		/// </summary>
		public static string LoverSuicidesMessage => loverSuicidesMessage.PickRandom();

		/// <summary>
		///    The message that is sent to every alive werewolf at every night.
		///    Used for announcing a vote to kill.
		/// </summary>
		public static string WerewolfNightVoteMessage => werewolfNightVoteMessage.PickRandom();

		public static string CupidVoteMessage => cupidVoteMessage.PickRandom();

		public static string GuardianAngelVoteMessage => guardianAngelVoteMessage.PickRandom();

		/// <summary>
		///    Announcement message that is sent to every oracle as a PM.
		///    Used for their choice dialog.
		/// </summary>
		public static string OracleInspectMessage => oracleInspectMessage.PickRandom();

		public static string WarlockInspectMessage => warlockInspectMessage.PickRandom();

		public static string GunnerVoteMessage => gunnerVoteMessage.PickRandom();

		/// <summary>
		///    Announcement of a werewolf kill. Used at the beginning of a morning state. <br/>
		///    It contains multiple messages, always returns a random one. <br/>
		///    Needs to be formatted with <see cref="string.Format(string, object[])"/>with the following parameters:
		/// <list type="number">
		///    <item>Name of the victim</item>
		/// </list> 
		/// </summary>
		public static string WerewolfKillMessage => werewolfKillMessages.PickRandom();

		/// <summary>
		/// 
		/// </summary>
		public static string GuardianAngelSavedMessage => guardianAngelSavedMessage.PickRandom();

		/// <summary>
		///    Personal message for when a cursed is bit by a werewolf and becomes one of them.
		/// </summary>
		public static string CursedTransformMessage => cursedTransformMessage.PickRandom();
		
		/// <summary>
		///    Announcement of a cursed turning into a werewolf to the other werewolves.<br/>
		///    It contains multiple messages, always returns a random one. <br/>
		///    Needs to be formatted with <see cref="string.Format(string, object[])"/>with the following parameters:
		/// <list type="number">
		///     <item>Name of the victim</item>
		/// </list> 
		/// </summary>
		public static string CursedTransformAnnounceMessage => cursedTransformAnnounceMessage.PickRandom();

		public static string WerewolfAteDrunkAnnounceMessage => werewolfAteDrunkAnnounceMessage.PickRandom();

		/// <summary>
		///    Announcement of a gunner kill.<br/>
		///    It contains multiple messages, always returns a random one.  <br/>
		///    Needs to be formatted with <see cref="string.Format(string, object[])"/>with the following parameters:
		/// <list type="number">
		///     <item>Name of the gunner</item>
		///     <item>Name of the victim</item>
		/// </list> 
		/// </summary>
		public static string GunnerShotMessage => gunnerShotMessages.PickRandom();

		public static string NoWerewolfKillMessage => noWerewolfKillMessage.PickRandom();

		/// <summary>
		///    Announcement of a lynched person. Used at the beginning of a morning state. <br/>
		///    It contains multiple messages, always returns a random one. <br/>
		///    Needs to be formatted with <see cref="string.Format(string, object[])"/>with the following parameters:
		/// <list type="number">
		///     <item>Name of the victim</item>
		/// </list> 
		/// </summary>
		public static string VillagerLynchMessage => villagerLynchMessages.PickRandom();

		public static string MasochistLynchMessage => masochistLynchMessage.PickRandom();

		/// <summary>
		///    The private message that is received when the oracle finds a villager. <br/>
		///    It contains multiple messages, always return a random one.<br/>
		///    Needs to be formatted with <see cref="string.Format(string, object[])"/>with the following parameters:
		/// <list type="number">
		///     <item>Name of the observed character.</item>
		/// </list>
		/// </summary>
		public static string OracleFindsVillagerMessages => oracleFindsVillagerMessages.PickRandom();

		/// <summary>
		///    The private message that is received when the oracle finds a werewolf. <br/>
		///    It contains multiple messages, always return a random one.<br/>
		///    Needs to be formatted with <see cref="string.Format(string, object[])"/>with the following parameters:
		/// <list type="number">
		///     <item>Name of the observed character.</item>
		/// </list>   
		/// </summary>
		public static string OracleFindsWerewolfMessages => oracleFindsWerewolfMessages.PickRandom();

		/// <summary>
		///    The private message that is received when the oracle finds an drunk. <br/>
		///    It contains multiple messages, always return a random one.<br/>
		///    Needs to be formatted with <see cref="string.Format(string, object[])"/>with the following parameters:
		/// <list type="number">
		/// 	<item>Name of the observed character.</item>
		/// </list>   
		/// </summary>
		public static string OracleFindsDrunkMessages => oracleFindsDrunkMessages.PickRandom();

		/// <summary>
		///    The private message that is received when the oracle finds an oracle. <br/>
		///    It contains multiple messages, always return a random one.<br/>
		///    Needs to be formatted with <see cref="string.Format(string, object[])"/>with the following parameters:
		/// <list type="number">
		///    <item>Name of the observed character.</item>
		/// </list>
		/// </summary>
		public static string OracleFindsOracleMessages => oracleFindsOracleMessages.PickRandom();

		/// <summary>
		///    The private message that is received when the oracle finds a gunner. <br/>
		///    It contains multiple messages, always return a random one.<br/>
		///    Needs to be formatted with <see cref="string.Format(string, object[])"/>with the following parameters:
		/// <list type="number">
		/// 	<item>Name of the observed character.</item>
		/// </list>
		/// </summary>
		public static string OracleFindsGunnerMessages => oracleFindsGunnerMessages.PickRandom();

		/// <summary>
		///    The private message that is received when the oracle finds a cursed. <br/>
		///    It contains multiple messages, always return a random one.<br/>
		///    Needs to be formatted with <see cref="string.Format(string, object[])"/>with the following parameters:
		/// <list type="number">
		///    <item>Name of the observed character.</item>
		/// </list>
		/// </summary>
		public static string OracleFindsCursedMessages => oracleFindsCursedMessages.PickRandom();

		/// <summary>
		///    The private message that is received when the oracle finds a cupid. <br/>
		///    It contains multiple messages, always return a random one.<br/>
		///    Needs to be formatted with <see cref="string.Format(string, object[])"/>with the following parameters:
		/// <list type="number">
		/// 	<item>Name of the observed character.</item>
		/// </list>
		/// </summary>
		public static string OracleFindsCupidMessages => oracleFindsCupidMessages.PickRandom();

		/// <summary>
		///    The private message that is received when the oracle finds a guardian angel. <br/>
		///    It contains multiple messages, always return a random one.<br/>
		///    Needs to be formatted with <see cref="string.Format(string, object[])"/>with the following parameters:
		/// <list type="number">
		/// 	<item>Name of the observed character.</item>
		/// </list>
		/// </summary>
		public static string OracleFindsGuardianAngelMessages => oracleFindsGuardianAngelMessages.PickRandom();

		/// <summary>
		///    The private message that is received when the oracle finds a warlock. <br/>
		///    It contains multiple messages, always return a random one.<br/>
		///    Needs to be formatted with <see cref="string.Format(string, object[])"/>with the following parameters:
		/// <list type="number">
		/// 	<item>Name of the observed character.</item>
		/// </list>
		/// </summary>
		public static string OracleFindsWarlockMessages => oracleFindsWarlockMessages.PickRandom();


		/// <summary>
		///    The private message that is received when the warlock finds a villager. <br/>
		///    It contains multiple messages, always return a random one.<br/>
		///    Needs to be formatted with <see cref="string.Format(string, object[])"/>with the following parameters:
		/// <list type="number">
		/// 	<item>Name of the observed character.</item>
		/// </list>
		/// </summary>
		public static string WarlockFindsVillagerMessages => warlockFindsVillagerMessages.PickRandom();

		/// <summary>
		///    The private message that is received when the warlock finds a werewolf. <br/>
		///    It contains multiple messages, always return a random one.<br/>
		///    Needs to be formatted with <see cref="string.Format(string, object[])"/>with the following parameters:
		/// <list type="number">
		/// 	<item>Name of the observed character.</item>
		/// </list>   
		/// </summary>
		public static string WarlockFindsWerewolfMessages => warlockFindsWerewolfMessages.PickRandom();

		/// <summary>
		///    The private message that is received when the warlock finds a drunk. <br/>
		///    It contains multiple messages, always return a random one.<br/>
		///    Needs to be formatted with <see cref="string.Format(string, object[])"/>with the following parameters:
		/// <list type="number">
		/// 	<item>Name of the observed character.</item>
		/// </list>   
		/// </summary>
		public static string WarlockFindsDrunkMessages => warlockFindsDrunkMessages.PickRandom();

		/// <summary>
		/// 	The private message that is received when the warlock finds an oracle. <br/>
		/// 	It contains multiple messages, always return a random one.<br/>
		/// 	Needs to be formatted with <see cref="string.Format(string, object[])"/>with the following parameters:
		/// <list type="number">
		/// 	<item>Name of the observed character.</item>
		/// </list>
		/// </summary>
		public static string WarlockFindsOracleMessages => warlockFindsOracleMessages.PickRandom();

		/// <summary>
		/// 	The private message that is received when the warlock finds a gunner. <br/>
		/// 	It contains multiple messages, always return a random one.<br/>
		/// 	Needs to be formatted with <see cref="string.Format(string, object[])"/>with the following parameters:
		/// <list type="number">
		/// 	<item>Name of the observed character.</item>
		/// </list>
		/// </summary>
		public static string WarlockFindsGunnerMessages => warlockFindsGunnerMessages.PickRandom();

		/// <summary>
		/// 	The private message that is received when the warlock finds a cursed. <br/>
		/// 	It contains multiple messages, always return a random one.<br/>
		/// 	Needs to be formatted with <see cref="string.Format(string, object[])"/>with the following parameters:
		/// <list type="number">
		/// 	<item>Name of the observed character.</item>
		/// </list>
		/// </summary>
		public static string WarlockFindsCursedMessages => warlockFindsCursedMessages.PickRandom();

		/// <summary>
		/// 	The private message that is received when the warlock finds a cupid. <br/>
		/// 	It contains multiple messages, always return a random one.<br/>
		/// 	Needs to be formatted with <see cref="string.Format(string, object[])"/>with the following parameters:
		/// <list type="number">
		/// 	<item>Name of the observed character.</item>
		/// </list>
		/// </summary>
		public static string WarlockFindsCupidMessages => warlockFindsCupidMessages.PickRandom();

		/// <summary>
		/// 	The private message that is received when the warlock finds a guardian angel. <br/>
		/// 	It contains multiple messages, always return a random one.<br/>
		/// 	Needs to be formatted with <see cref="string.Format(string, object[])"/>with the following parameters:
		/// <list type="number">
		/// 	<item>Name of the observed character.</item>
		/// </list>
		/// </summary>
		public static string WarlockFindsGuardianAngelMessages => warlockFindsGuardianAngelMessages.PickRandom();

		/// <summary>
		///    The private message that is received when the warlock finds a warlock. <br/>
		///    It contains multiple messages, always return a random one.<br/>
		///    Needs to be formatted with <see cref="string.Format(string, object[])"/>with the following parameters:
		/// <list type="number">
		///    <item>Name of the observed character.</item>
		/// </list>
		/// </summary>
		public static string WarlockFindsWarlockMessages => warlockFindsWarlockMessages.PickRandom();

		/// <summary>
		///    Announcement of the lynch event that ended with no kill.
		/// </summary>
		public static string NoLynchMessage => noLynchMessage.PickRandom();

		public static string AnnounceDayVotingMessage => announceDayVotingMessage.PickRandom();

		/// <summary>
		///    The message that is sent to every character as a PM to make them vote for a kill
		/// </summary>
		public static string DayVoteDMessage => dayVoteDMessage.PickRandom();

		public static string WerewolfWinMessage => werewolfWinMessage.PickRandom();

		public static string VillagerWinMessage => villagerWinMessage.PickRandom();

		public static string MasochistWinMessage => masochistWinMessage.PickRandom();

		public const string GuildJoinInfoMessage = "Werewolf bot has been invited to the server!\n" +
		                                           "You can use the following commands:\n" + 
		                                           " - TODO...";

		public const string TODO = "TODO";

		#endregion
	}
}