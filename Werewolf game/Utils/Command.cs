using System;
using System.Linq;
using Configurations;
using Discord.WebSocket;
using static Configurations.PropertyContainer.PropertyKey;

namespace Werewolf_game.Utils {
    public class Command {
        private static readonly log4net.ILog Logger =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        public bool IsCommand { get; }
        public string CommandString { get; }
        public string[] Arguments { get; }
        public ISocketMessageChannel Channel { get; }
        public SocketUser Author { get; }
        public SocketMessage Message { get; }

        private Command() {
            IsCommand = false;
        }

        private Command(string commandString, string[] arguments, SocketMessage message) {
            CommandString = commandString;
            Arguments = arguments;
            Channel = message.Channel;
            Author = message.Author;
            Message = message;
            IsCommand = true;
        }


        public static Command ParseCommand(SocketMessage msg) {
            try {
                var commandPrefix = PropertyContainer.GetInstance().GetProperty(CommandPrefix);
                if (string.IsNullOrEmpty(msg.Content) ||
                    !msg.Content.StartsWith(commandPrefix))
                    return new Command();
                //Setting command and argument variables.
                var command = msg.Content;
                command = command.Remove(0, commandPrefix.Length);
                var arguments = command.Split(' ');
                command = arguments[0];
                arguments = arguments.Skip(1).ToArray();

                return command.Length > 0 ? new Command(command, arguments, msg) : new Command();
            }
            catch (Exception e) {
                Logger.Warn($"Unexpected exception while parsing command. {e.Message}", e);
            }
            return new Command();
        }

        public override string ToString() {
            return $"{nameof(IsCommand)}: {IsCommand}, {nameof(CommandString)}: {CommandString}, {nameof(Arguments)}: {Arguments}, {nameof(Channel)}: {Channel}, {nameof(Author)}: {Author}, {nameof(Message)}: {Message}";
        }
    }
}