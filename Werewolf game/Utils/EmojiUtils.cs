using Discord;

namespace Werewolf_game.Utils {
    public static class EmojiUtils {
        private static readonly log4net.ILog Logger =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        public static IEmote LeftEmoji => new Emoji("⬅");
        public static IEmote RightEmoji => new Emoji("➡");
        
        /// <summary>
        /// Converts a digit to an emoji string. I know it's not really a digit because of 10.
        /// </summary>
        /// <param name="digit">The digit.</param>
        /// <returns>The string value of the related digit emoji if it's the digit is an integer between 1 and 10,
        /// or the string value of the ? digit otherwise.</returns>
        public static string DigitToEmoji(int digit) {
            switch (digit) {
                case 1:
                    return"\U00000031\U000020e3";
                case 2:
                    return"\U00000032\U000020e3";
                case 3:
                    return"\U00000033\U000020e3";
                case 4:
                    return"\U00000034\U000020e3";
                case 5:
                    return"\U00000035\U000020e3";
                case 6:
                    return"\U00000036\U000020e3";
                case 7:
                    return"\U00000037\U000020e3";
                case 8:
                    return"\U00000038\U000020e3";
                case 9:
                    return"\U00000039\U000020e3";
                case 10:
                    return "🔟";
                default:
                    Logger.Warn($"Can't create emoji for digit: {digit}!");
                    return"❓";
            }
        }

        
        public static int EmojiToDigit(IEmote emoji) {
            if (Equals(emoji, new Emoji(DigitToEmoji(1))))
                return 1;
            if (Equals(emoji, new Emoji(DigitToEmoji(2))))
                return 2;
            if (Equals(emoji, new Emoji(DigitToEmoji(3))))
                return 3;
            if (Equals(emoji, new Emoji(DigitToEmoji(4))))
                return 4;
            if (Equals(emoji, new Emoji(DigitToEmoji(5))))
                return 5;
            if (Equals(emoji, new Emoji(DigitToEmoji(6))))
                return 6;
            if (Equals(emoji, new Emoji(DigitToEmoji(7))))
                return 7;
            if (Equals(emoji, new Emoji(DigitToEmoji(8))))
                return 8;
            if (Equals(emoji, new Emoji(DigitToEmoji(9))))
                return 9;
            if (Equals(emoji, new Emoji(DigitToEmoji(10))))
                return 10;

            Logger.Warn($"Can't convert {emoji} to digit. Returning 0");
            return 0;
        }
    }
}