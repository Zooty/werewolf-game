using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Discord;
using Discord.Rest;

namespace Werewolf_game.Utils {
    public static class DiscordUtils {
        private static readonly log4net.ILog Logger =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        
        /// <summary>
        /// Removes all reactions from the given users on a message.
        /// You probably want to call <see cref="RestMessage.UpdateAsync"/> to ensure all reactions are visible.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="users"></param>
        public static async Task removeAllReactionsAsync(IUserMessage message, params IUser[] users) {
            //await message.RemoveAllReactionsAsync() //Wish this would work...
            
            
            var removeTasks = new Collection<Task>();
            //First starting every removal tasks to let them all run.
            foreach (var reaction in message.Reactions.Keys) {
                foreach (var user in users) {
                    Logger.Debug($"Removing reaction events from {user}");
                    removeTasks.Add(message.RemoveReactionAsync(reaction, user));
                }
            }
            //Then wait for all of them to finish
            foreach (var removeTask in removeTasks) {
                await removeTask;
            }
        }

        public static async Task<bool> IsDMOpen(IUser user, string message = "Test DM") {
            try {
                var testMessage = await user.SendMessageAsync(message);
                #pragma warning disable 4014
                testMessage.DeleteAsync();
                #pragma warning restore 4014
                return true;
            }
            catch (Exception) {
                return false;
            } 
        }
    }
}