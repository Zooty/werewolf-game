using System.Linq;
using Discord.WebSocket;

namespace Werewolf_game.Persistency {
	public class UserData {
		private readonly string name;
		public bool Played { get; }
		public int GamesPlayed { get; }
		public int GamesWon { get; }
		public int DiedFirst { get; }

		public UserData(SocketUser user) {
			name = user.Username;
			var resultList = DatabaseWrapper.DbContext.Players.Where(player => player.Id == user.Id).ToList();
			if (resultList.Count == 1) {
				var player = resultList[0];
				Played = true;
				GamesPlayed = player.GameTimes;
				GamesWon = player.WonTimes;
				DiedFirst = player.FirstBloodTimes;
			}
			else {
				Played = false;
			}
		}

		public string StatusReport() {
			return 
				Played
				? $"Status for {name}:\n" +
				  $"Played {GamesPlayed} games\n" +
				  $"Won {GamesWon} times\n" +
				  $"Died first {DiedFirst} times."
				: $"{name} have not played any games yet";
		}
		public override string ToString() {
			return $"{nameof(name)}: {name}, {nameof(Played)}: {Played}, {nameof(GamesPlayed)}: {GamesPlayed}, {nameof(GamesWon)}: {GamesWon}, {nameof(DiedFirst)}: {DiedFirst}";
		}
	}
}