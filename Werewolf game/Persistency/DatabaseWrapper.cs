using DatabaseConnection;
using Werewolf_game.Exceptions;

namespace Werewolf_game.Persistency {
	public static class DatabaseWrapper {
		private static readonly log4net.ILog Logger =
			log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		
		private static WerewolfDbContext dbContext;

		public static WerewolfDbContext DbContext {
			get {
				checkInitialized();
				return dbContext;
			}
			private set => dbContext = value;
		}

		public static void init() {
			DbContext = new WerewolfDbContextFactory().CreateDbContext();
			Logger.Debug("Database context is created. Ensuring database creation.");
			dbContext.Database.EnsureCreated();  
		}

		private static void checkInitialized() {
			if (dbContext == null) {
				throw new NotInitializedException("DatabaseConnection is not initialized. Call init()!");
			}
		}
	}
}