using System;
using System.Collections.Generic;
using System.Linq;
using DatabaseConnection.Entities;
using Discord.WebSocket;
using Werewolf_game.WerewolfBot.Characters;

namespace Werewolf_game.Persistency {
	public class GameSessionData {
		private static readonly log4net.ILog Logger =
			log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		private Result sessionResult;

		public ISocketMessageChannel Channel { get; }

		public Result SessionResult {
			get => sessionResult;
			set {
				sessionResult = value;
				winners.Clear();
				switch (value) {
					case Result.WerewolfWin:
						foreach (var character in Players) {
							if (character.IsAlive && character.CharacterTeam == Character.Side.Werewolves) {
								winners.Add(character);
								if (character.Lover != null && character.Lover.IsAlive) {
									winners.Add(character.Lover);
								}
							}
						}
						break;
					case Result.VillagerWin:
						foreach (var character in Players) {
							if (character.IsAlive && character.CharacterTeam == Character.Side.Villagers) {
								winners.Add(character);
								if (character.Lover != null && character.Lover.IsAlive) {
									winners.Add(character.Lover);
								}
							}
						}
						break;
					case Result.MasochistWin:
						var masochist = Players.Single(character => character.CharacterType == Character.Type.Masochist);
						winners.Add(masochist);
						if(masochist.Lover != null && masochist.Lover.IsAlive) {
							winners.Add(masochist);
						}
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(value), value, null);
				}
			}
		}

		public Character FirstKill { get; set; }
		public ICollection<Character> Players { get; set; }

		private readonly ICollection<Character> winners = new HashSet<Character>();

		public int Days { get; set; }

		public GameSessionData(ISocketMessageChannel channel) {
			Channel = channel;
		}


		public void Save() {
			var dbContext = DatabaseWrapper.DbContext;
			try {
				//throw new NotImplementedException();
				Logger.Debug($"Saving game session data to database in {Channel}");
				var gameSession = new GameSession {Id = Guid.NewGuid().ToString(), Time = DateTime.Now, Days = Days};
				dbContext.GameSessions.Add(gameSession);
				
				var dbPlayers = dbContext.Players;
				foreach (var character in Players) {
					var playersFromDb = dbPlayers.Where(player => player.Id == character.user.Id).ToList();
					Player playerFromDb;
					if (playersFromDb.Count == 0) {
						playerFromDb = new Player {
							Id = character.user.Id,
							GameTimes = 1,
							FirstBloodTimes = Equals(FirstKill, character) ? 1 : 0,
							WonTimes = winners.Contains(character) ? 1 : 0
						};
						dbPlayers.Add(playerFromDb);
					}
					else {
						playerFromDb = playersFromDb[0];
						playerFromDb.GameTimes++;
						playerFromDb.FirstBloodTimes = Equals(FirstKill, character) ? playerFromDb.FirstBloodTimes + 1 : playerFromDb.FirstBloodTimes; 
						playerFromDb.WonTimes = winners.Contains(character) ? playerFromDb.WonTimes + 1 : playerFromDb.WonTimes;
					}

					dbContext.PlayerGameSessions.Add(new PlayerGameSession {GameSession = gameSession, Player = playerFromDb});
				}

				dbContext.SaveChanges();
				Logger.Info($"Game session data are saved for {Channel}");
			}
			catch (Exception e) {
				Logger.Error("Database Error while saving game session data!", e);
				Logger.Error($"Game session data: {ToString()}");
				Logger.Error($"Database context: {dbContext}");
			}

			//throw new NotImplementedException(); 
		}
		
		public enum Result {
			WerewolfWin,
			VillagerWin,
			MasochistWin
		}
		public override string ToString() {
			return $"{nameof(Channel)}: {Channel}, {nameof(SessionResult)}: {SessionResult}, {nameof(FirstKill)}: {FirstKill}, {nameof(Players)}: {Players}, {nameof(Days)}: {Days}";
		}
	}
}