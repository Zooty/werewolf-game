using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using Werewolf_game.Extensions;

namespace Configurations {
    public class PropertyContainer {
        private static readonly log4net.ILog Logger =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly Dictionary<string, string> properties = new Dictionary<string, string>();

        private static PropertyContainer instance;

        private PropertyContainer() {
            Logger.Debug("Loading configs from config.properties");
            try {
                foreach (var row in File.ReadAllLines("config.properties")) {
                    try {
                        var splitRow = row.Split('=');
                        if (row.Length > 0 && row[0] != '#' && splitRow.Length > 1) {
                            properties[splitRow[0].Trim()] =
                                string.Join("=", splitRow.Skip(1).ToArray()).Trim();
                        }
                    }
                    catch (Exception e) {
                        Logger.Warn($"Couldn't parse line: {row}. {e.Message}", e);
                    }
                }
            }
            catch (Exception e) {
                Logger.Error("Couldn't load properties from config.properties!", e);
                throw;
            }

            Logger.Info($"Loaded {properties.Count} properties.");
        }

        public static PropertyContainer GetInstance() {
            return instance ?? (instance = new PropertyContainer());
        }

        public string GetProperty(PropertyKey key) {
            return properties[key.GetValue()];
        }

        public int GetIntPropertySafe(PropertyKey key, int defaultValue = 0) {
            var prop = properties[key.GetValue()];
            if (!int.TryParse(prop, out int result)) {
                Logger.Warn($"Couldn't parse {key.GetValue()}({prop}) to integer!");
                return defaultValue;
            }

            return result;
        }

        public double GetDoublePropertySafe(PropertyKey key, double defaultValue = 0) {
            var prop = properties[key.GetValue()];
            if (!double.TryParse(prop, NumberStyles.Any, CultureInfo.InvariantCulture, out double result)) {
                Logger.Warn($"Couldn't parse {key.GetValue()}: {prop} to double!");
                return defaultValue;
            }

            return result;
        }

        /// <summary>
        /// Erases properties from memory. Forcing it to load from file again.
        /// </summary>
        public static void Reset() {
            instance = null;
        }

        public enum PropertyKey {
            [Description("DatabaseConnection")]
            DbConnectionString,
            
            [Description("Token")] 
            Token,

            [Description("Werewolf.CommandPrefix")]
            CommandPrefix,
            
            [Description("Werewolf.Game.DayLength")]
            DayLength,

            [Description("Werewolf.Game.DayVoteLength")]
            DayVoteLength,

            [Description("Werewolf.Game.NightVoteLength")]
            NightVoteLength,
            
            [Description("Werewolf.Game.RoleAssignWaitLength")]
            RoleAssignWaitLength,
            
            [Description("Werewolf.Game.JoinWaitLength")]
            JoinWaitLength,

            [Description("Werewolf.Game.MinimumPlayers")]
            MinPlayers,

            [Description("Werewolf.Game.WerewolfRatio")]
            WerewolfRatio,
            
            [Description("Werewolf.Game.OracleRatio")]
            OracleRatio,
            
            [Description("Werewolf.Game.AdditionalOracleChance")]
            AdditionalOracleChance,
            
            [Description("Werewolf.Game.WarlockRatio")]
            WarlockRatio,
            
            [Description("Werewolf.Game.AdditionalWarlockChance")]
            AdditionalWarlockChance,
            
            [Description("Werewolf.Game.DrunkRatio")]
            DrunkRatio,
            
            [Description("Werewolf.Game.AdditionalDrunkChance")]
            AdditionalDrunkChance,
            
            [Description("Werewolf.Game.GunnerRatio")]
            GunnerRatio,
            
            [Description("Werewolf.Game.AdditionalGunnerChance")]
            AdditionalGunnerChance,
            
            [Description("Werewolf.Game.CupidChance")]
            CupidChance,
            
            [Description("Werewolf.Game.CursedRatio")]
            CursedRatio,
            
            [Description("Werewolf.Game.AdditionalCursedChance")]
            AdditionalCursedChance,
            
            [Description("Werewolf.Game.GuardianAngelRatio")]
            GuardianAngelRatio,
            
            [Description("Werewolf.Game.AdditionalGuardianAngelChance")]
            AdditionalGuardianAngelChance,
                          
            [Description("Werewolf.Game.MasochistRatio")]
            MasochistRatio,
            
            [Description("Werewolf.Game.AdditionalMasochistChance")]
            AdditionalMasochistChance,
        }
    }
}