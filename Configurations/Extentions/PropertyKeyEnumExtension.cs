using System;
using System.ComponentModel;
using Configurations;

namespace Werewolf_game.Extensions {
    
    /// <summary>
    /// Gives a string value for <see cref="PropertyContainer.PropertyKey">PropertyKey</see> enum.
    /// </summary>
    public static class PropertyKeyEnumExtension {
        
        /// <summary>
        /// Gets the string value of the Enum.
        /// </summary>
        /// <param name="key">Reference to the Enum class.</param>
        /// <returns>The string value of the enum. Equals the value of the Description attribute if set, or the name of the enum if not set.</returns>
        public static string GetValue(this Enum key) {
            var attributes = (DescriptionAttribute[]) key
                .GetType()
                .GetField(key.ToString())
                .GetCustomAttributes(typeof(DescriptionAttribute), false);

            return attributes.Length > 0
                ? attributes[0].Description
                : Enum.GetName(typeof(PropertyContainer.PropertyKey), key);
        }
    }
}